import playJessibuca from './playH5-jessibuca';
import deptCarTreeMultOfPM from './deptCarTreeMultOfPM';
import selectDeptCarInput from './selectDeptCarInput';
/* istanbul ignore next */
const commentComponent = {
  install: function(Vue) {
    Vue.component(playJessibuca.name, playJessibuca);
    Vue.component(deptCarTreeMultOfPM.name, deptCarTreeMultOfPM);
    Vue.component(selectDeptCarInput.name, selectDeptCarInput);
  }
};
export default commentComponent;