// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Api from "@/api/api.js"
import 'element-ui/lib/theme-chalk/index.css'//新添加
import ElementUI from 'element-ui' //新添加
Vue.use(ElementUI);//新添加
import Ztree from 'ztree' //新添加
import 'ztree/css/ztreeStyle/zTreeStyle.css'//新添加
Vue.use(Ztree);//新添加
import commentComponent from './components' //新添加-引用该文件内的所有组件
Vue.use(commentComponent)   //新添加
Vue.config.productionTip = false
Vue.prototype.$http = Api;
import utils from '@/utils/utils.js';
Vue.prototype.randomStr = utils.randomStr; // 生成n位长度的字符串
Vue.prototype.loadJS = utils.loadJS;//动态引入js
Vue.prototype.loadStyles = utils.loadStyles;//动态引入css
Vue.prototype.formatTime = utils.formatTime;//时间格式过滤器
Vue.filter("formatTime",utils.formatTime);//时间格式过滤器
Vue.filter("date_format",utils.date_format);//"210524165408"=>"2021-05-24 16:54:08"
Vue.prototype.date_format = utils.date_format;//"210524165408"=>"2021-05-24 16:54:08"
Vue.prototype.tranlateTime = utils.tranlateTime;//转换秒的显示-->>>n天n时n分n秒
Vue.filter("tranlateTime",utils.tranlateTime);//转换秒的显示-->>>n天n时n分n秒
Vue.prototype.toDownLoadImg = utils.toDownLoadImg;//多浏览器图片下载
Vue.prototype.compareObj = utils.compareObj;//对讲数组按arg降序排列
router.beforeEach((to,from,next) => {
  let specialPath = !!to.path&&to.path!=="/"&&to.path!=="/login";
  if(specialPath){
    next();
  }else{
    next();
  }
});
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
