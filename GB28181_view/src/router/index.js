import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/pages/login'
import Contain from '@/pages/contain'
Vue.use(Router)
//先把VueRouter原型对象的push保存一份
const originPush = Router.prototype.push
//重写push方法 
Router.prototype.push = function (location,resolve,reject) {
  //调用保存的push方法
  //但是保存的push方法是挂载在window的方法 所以要通过call修改this的指向
  if(resolve&&reject){
    originPush.call(this,location,resolve,reject);
  }else{
    originPush.call(this,location,()=>{},()=>{});
  }
}
const originReplace = Router.prototype.replace
//重写replace方法 
Router.prototype.replace = function (location,resolve,reject) {
  //调用保存的replace方法
  //但是保存的replace方法是挂载在window的方法 所以要通过call修改this的指向
  if(resolve&&reject){
    originReplace.call(this,location,resolve,reject);
  }else{
    originReplace.call(this,location,()=>{},()=>{});
  }
}
export default new Router({
  mode: "hash",//hash||history
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        auth: false,
        keepAlive: false
      }
    },{
      path: '/',
      name: 'contain',
      meta: {
        auth: true
      },
      redirect: { name: 'login' },
      component: Contain,
      children:[
        {// 位置监控
          path: '/livePreview',
          name: 'livePreview',
          meta: {
            auth: true,
            keepAlive: false
          },
          component: resolve => require(['@/pages/contain/livePreview'], resolve)
        },
        {// 历史视频
          path: '/replayVideo',
          name: 'replayVideo',
          meta: {
            auth: true,
            keepAlive: false
          },
          component: resolve => require(['@/pages/contain/replayVideo'], resolve)
        }
      ]
    }
  ]
})
