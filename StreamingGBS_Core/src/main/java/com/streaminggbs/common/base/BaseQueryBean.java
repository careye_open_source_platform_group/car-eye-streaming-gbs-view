package com.streaminggbs.common.base;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author LiLin
 * @Date 2020/9/11 14:53
 * @Description 列表查询公共接收类
 */
@Data
public class BaseQueryBean implements Serializable {
    private static final long serialVersionUID = 1L;

    /****基础查询条件****/
    private String q;

    private String sort;

    private String order;


    /****特殊查询条件****/
    private Integer online;

    private String deviceid;

    private Integer type;

    private String deptidAllChild;//包含子机构

    private String deptid;

    private String deptidnew;

    private String belongDeptid;

    /****报警查询条件****/
    private Integer alarmtype;

    private String startDate;

    private String endDate;


}
