package com.streaminggbs.common.base;


/**
 * 
 * @author lilin
 *
 */
public class ErrorCodeException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private int code;
    private String msg;
    private String exception;

    public ErrorCodeException(String msg) {
        this(-9999, msg);
    }
    public ErrorCodeException(int code, String msg) {
        this(code, msg, "");
    }

    public ErrorCodeException(int code, String msg, String exception) {
        this.code = code;
        this.msg = msg;
        this.exception = exception;
    }

    public ErrorCodeException(String msg,Exception e) {
        this(9990,msg + ";"+  e.getMessage(), e.getLocalizedMessage());
    }

    public ErrorCodeException(Exception e) {
        this(9990, e.getMessage(), e.getLocalizedMessage());
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String getMessage(){
        return this.getMsg() + " " + this.getException();
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}
