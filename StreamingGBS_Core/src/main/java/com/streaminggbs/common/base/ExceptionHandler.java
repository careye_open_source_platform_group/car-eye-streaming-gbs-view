package com.streaminggbs.common.base;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author lilin
 */
@ControllerAdvice
@Slf4j
public class ExceptionHandler {
	

	@org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<ReturnBean> defaultErrorHandler(Exception e) {
        log.error("Unknown Exception", e);
		return new ResponseEntity<>(new ReturnBean(-999, "Unknown Exception"), HttpStatus.OK);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(TypeMismatchException.class)
    @ResponseBody
    public ResponseEntity<ReturnBean> typeMismatchExceptionHandler(TypeMismatchException e) {
        log.error("参数类型错误：", e);
		return new ResponseEntity<>(new ReturnBean(-998, "参数类型错误：" +e.getMessage()), HttpStatus.OK);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    public ResponseEntity<ReturnBean> httpRequestMethodNotSupportedExceptionHandler(HttpRequestMethodNotSupportedException e) {
        log.error("不支持该方法：", e);
		return new ResponseEntity<>(new ReturnBean(-999, "不支持该方法："+e.getMessage()), HttpStatus.OK);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseBody
    public ResponseEntity<ReturnBean> missingServletRequestParameterExceptionExceptionHandler(MissingServletRequestParameterException e) {
        log.error("参数未找到：", e);
		return new ResponseEntity<>(new ReturnBean(-999, "参数未找到："+e.getMessage()), HttpStatus.OK);
    }
    
    @org.springframework.web.bind.annotation.ExceptionHandler(ErrorCodeException.class)
    @ResponseBody
    public ResponseEntity<ReturnBean> errorCodeExceptionErrorHandler(ErrorCodeException e) {
        log.error("错误参数：", e);
		return new ResponseEntity<>(new ReturnBean(e.getCode(), e.getMsg()), HttpStatus.OK);
    }
}
