package com.streaminggbs.common.base;


import com.streaminggbs.common.job.StreamCheckJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class StartLoader implements ApplicationRunner {
    @Autowired
    StreamCheckJob streamCheckJob;
    @Override
    public void run(ApplicationArguments args){
        loadSysParams();
    }
    public void loadSysParams(){
        System.out.println("【系统参数1】加载中...");
        streamCheckJob.updateCarStatus();
        System.out.println("【系统参数1】加载完成...");
    }
}
