package com.streaminggbs.common.base;

import com.github.pagehelper.PageInfo;
import org.springframework.http.HttpStatus;

import java.util.List;

/**
 * @author lilin
 * @date 2020年9月11日 下午2:00:19
 * @ClassName: ReturnBean
 * @Description: 提供接口，返回数据（返回数据，返回）
 */

public class VideoReturnBean {

	private int code = 0;
	private String msg = "";
	private Object data;
	private long count = 0;

	public VideoReturnBean() {
	}

	public VideoReturnBean(PageInfo list) {
		if (list != null && list.getList() != null && !list.getList().isEmpty()) {
			this.data = list.getList();
			this.count = list.getTotal();
		}
	}

	public VideoReturnBean(List list) {
		this(list, false);
	}

	public VideoReturnBean(List list, boolean allowEmpty) {
		this.data = list;
		this.count = list.size();
		if (!allowEmpty && list.isEmpty()) {
			return;
		}
		this.data = list;
		this.count = list.size();
	}

	public VideoReturnBean(Object data) {
		this.data = data;
	}

	public VideoReturnBean(HttpStatus status) {
		this.msg = status.getReasonPhrase();
		this.code = status.value();
	}

	public VideoReturnBean(List list, long count) {
		this.data = list;
		this.count = count;
	}

	public VideoReturnBean(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

}
