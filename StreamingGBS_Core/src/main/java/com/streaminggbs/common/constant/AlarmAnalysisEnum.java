package com.streaminggbs.common.constant;

/**
 * 报警枚举
 */
public enum AlarmAnalysisEnum {


    ALARM_TYPE_VIDEO_LOST(0x01,"视频丢失告警录像",0),
    ALARM_TYPE_DEVICE_LOST(0x02,"设备防拆报警",1),
    ALARM_TYPE_DEVICE_DISK_FULL(0x04,"存储设备磁盘满报警",2),
    ALARM_TYPE_DEVICE_TEMPERATURE_HIGH(0x08,"设备高温告警",3),
    ALARM_TYPE_DEVICE_TEMPERATURE_LOW(0x10,"设备低温告警",4),

    ALARM_TYPE_MANUAL_VIDEO(0x00100,"人工视频报警",5),
    ALARM_TYPE_MOTION_DETECT(0x00200,"运动目标检测报警",6),
    ALARM_TYPE_LOSE_DETECT(0x10000000 | 0x03,"遗留物检测报警",7),
    ALARM_TYPE_OBJECT_REMOVE_DETECT(0x10000000 | 0x04,"物体移除检测报警",8),
    ALARM_TYPE_AROUND_LINE_DETECT(0x10000000 | 0x05,"绊线检测报警",9),
    ALARM_TYPE_INVASION_DETECT(0x10000000 | 0x06,"入侵检测报警",10),
    ALARM_TYPE_RETROGRADE_DETECT(0x10000000 | 0x07,"逆行检测报警",11),
    ALARM_TYPE_WANDER_DETECT(0x10000000 | 0x08,"徘徊检测报警",12),
    ALARM_TYPE_FLOW_STATISTICAL_DETECT(0x10000000 | 0x09,"流量统计报警",13),
    ALARM_TYPE_DENSITY_DETECT(0x02000000 | 0x0A,"密度检测报警",14),
    ALARM_TYPE_VIDEO_ERROR_DETECT(0x10000000 | 0x0B,"视频异常检测报警",15),
    ALARM_TYPE_FAST_MOVE_DETECT(0x10000000 | 0x0C,"快速移动报警",16),
    ALARM_TYPE_DEVICE_DISK_FATAL(0x01000000,"磁盘故障告警",17),
    ALARM_TYPE_DEVICE_FAN_FATAL(0x02000000,"风扇故障告警",18),
    ALARM_TYPE_VIDEO_KEEP_FATAL(0x40000,"视频遮挡告警录像",19),
    ALARM_TYPE_STATUS_FATAL(0x10000000,"状态事件告警",20);

    private Integer alarmType;
    private Integer code;
    private String desc;

    AlarmAnalysisEnum(Integer code, String desc,Integer alarmType) {
        this.code = code;
        this.desc = desc;
        this.alarmType = alarmType;
    }

    /**
     * 自己定义一个静态方法,通过code返回枚举常量对象
     * @param code
     * @return
     */
    public static AlarmAnalysisEnum getAlarmAnalysis(Integer code){

        for (AlarmAnalysisEnum alarmAnalysisEnum: values()) {
            if(alarmAnalysisEnum.getCode().equals(code)){
                return  alarmAnalysisEnum;
            }
        }
        return null;
    }

    public Integer getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(Integer alarmType) {
        this.alarmType = alarmType;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
