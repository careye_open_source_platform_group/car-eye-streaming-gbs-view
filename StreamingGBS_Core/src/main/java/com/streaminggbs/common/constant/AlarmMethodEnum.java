package com.streaminggbs.common.constant;

/**
 * 报警枚举
 */
public enum AlarmMethodEnum {


    PHONE_ALARM(0x00300000,"电话报警",0),
    DEVICE_ALARM(0x000000FF,"设备报警",1),
    SMS_ALARM(0x00C00000,"短信报警",2),
    GPS_ALARM(0x0300000,"GPS报警",3),
    VIDEO_ALARM(0x000FFF00,"视频报警",4),
    DEVICE_FATAL_ALARM(0x0C000000,"设备故障报警",5),
    OTHER_ALARM(0xF0000000,"其他报警",6);


    private Integer methodType;
    private Integer code;
    private String desc;

    AlarmMethodEnum(Integer code, String desc,Integer methodType) {
        this.code = code;
        this.desc = desc;
        this.methodType = methodType;
    }

    public static void main(String[] args) {
        System.out.println(512&0x0300000);
    }
    /**
     * 自己定义一个静态方法,通过code返回枚举常量对象
     * @param code
     * @return
     */
    public static AlarmMethodEnum getAlarmMethod(Integer code){
        for (AlarmMethodEnum alarmMethodEnum: values()) {
            if(alarmMethodEnum.getCode().intValue()==(code & 0xff000000)){
                return  alarmMethodEnum;
            }
        }
        return null;
    }

    /**
     * 查询到报警大类信息
     * @param code
     * @return
     */
    public static AlarmMethodEnum getAlarmMethodByCode(Integer code) {
        for (AlarmMethodEnum alarmMethodEnum: values()) {
            if((code & alarmMethodEnum.getCode()) != 0){
                return  alarmMethodEnum;
            }
        }
        return null;
    }

    /**
     * 查询到报警大类信息
     * @param methodType
     * @return
     */
    public static AlarmMethodEnum getAlarmMethodByMethodType(Integer methodType) {
        for (AlarmMethodEnum alarmMethodEnum: values()) {
            if(methodType.equals(alarmMethodEnum.methodType)){
                return  alarmMethodEnum;
            }
        }
        return null;
    }

    public Integer getMethodType() {
        return methodType;
    }

    public void setMethodType(Integer methodType) {
        this.methodType = methodType;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
