package com.streaminggbs.common.constants;


public class Constants {

    public static final String LIVE_PLAY = "/v1/live/start";
    //public static final String LIVE_SPEAK = "/v1/live/speak";
    public static final String LIVE_STOP = "/v1/live/stop";
    public static final String VOD_START = "/v1/vod/start";
    public static final String VOD_STOP = "/v1/vod/stop";
    public static final String VOD_QUERY = "/v1/vod/query";
    // 设备 - 回放-seek
    public static final String VOD_CONTROL = "/v1/vod/control";
    // 设备重启
    public static final String DEVICE_REBOOT = "/v1/device/reboot";
    // 报警重置
    public static final String ALARM_RESET = "/v1/alarm/reset";
    // 设备 - 云台控制
    public static final String PTZ_CONTROL = "/v1/ptz/control";

    //布防
    public static final String alarmSetguard = "/v1/alarm/setguard";
    //取消
    public static final String alarmResetguard = "/v1/alarm/resetguard";
    // 云台控制的方向
    public static final String PTZ_CONTROL_LEFT = "left";
    public static final String PTZ_CONTROL_RIGHT = "right";
    public static final String PTZ_CONTROL_UP = "up";
    public static final String PTZ_CONTROL_DOWN = "down";
    public static final String PTZ_CONTROL_UPLEFT = "upleft";
    public static final String PTZ_CONTROL_UPRIGHT = "upright";
    public static final String PTZ_CONTROL_DOWNLEFT = "downleft";
    public static final String PTZ_CONTROL_DOWNRIGHT = "downright";
    public static final String PTZ_CONTROL_STOP = "stop";
    public static final String PTZ_ZOOM_IN = "in";
    public static final String PTZ_ZOOM_OUT = "out";
    public static final String PTZ_FOCUS_FAR = "far";
    public static final String PTZ_FOCUS_NEAR = "near";
    // 上下线状态
    public static final String DEVICE_STATUS_ONLINE = "EventOnline";
    public static final String DEVICE_STATUS_OFFLINE = "EventOffline";
    // 最后位置通知
    public static final String DEVICE_LAST_LOCATION = "EventMobilePosition";
    // 添加设备
    public static final String DEVICE_ADD = "/v1/device/add";
    // 删除设备
    public static final String DEVICE_DELETE = "/v1/device/delete";
    // 平台 - 查询所有设备和通道
    public static final String PLATFORM_DEVICE_CHANNEL_LIST = "/v1/platform/device/channel/list";
    // 设备 - 位置订阅
    public static final String PLATFORM_DEVICE_POSITION_SUBSCRIBE = "/v1/position/subscribe";
    // 设备 - 取消位置订阅
    public static final String PLATFORM_DEVICE_CANCEL_POSITION_SUBSCRIBE = "/v1/position/cancel";
    // 设备 - 实时录像开启
    public static final String PLATFORM_DEVICE_START_REAL_TIME_VIDEO = "/v1/record/start";
    // 设备 - 实时录像关闭
    public static final String PLATFORM_DEVICE_STOP_REAL_TIME_VIDEO = "/v1/record/stop";
    // 设备 - 开始对讲
    public static final String PLATFORM_DEVICE_START_SPEAK = "/v1/live/speak";
    // 设备 - 下载录像TO服务器
    public static final String PLATFORM_DEVICE_DOWNLOAD_RECORD = "/v1/record/download";
    // 设备 - 下载进度查询
    public static final String PLATFORM_DEVICE_DOWNLOAD_PROGRESS = "/v1/download/progress";
    // 设备 - 下载记录查询
    public static final String PLATFORM_DEVICE_DOWNLOAD_QUERY_RECORD = "/v1/record/list";
    // 设备 - 广播
    public static final String PLATFORM_DEVICE_START_BROADCAST = "/v1/live/broadcast";
    // 录像计划-停止
    public static final String PLAN_SCHEDULE_STOP = "/v1/record/schedule/time-stop";
    // 录像计划-开始
    public static final String PLAN_SCHEDULE_START = "/v1/record/schedule/time";
    // 设备-通道
    public static final String DEVICE_CHANNEL = "/v1/device/channel";

    //####################################################设备参数常量######################################################
    // 设备在线状态
    public static final String PLATFORM_DEVICE_ONLINE = "ONLINE";// 在线
    public static final String PLATFORM_DEVICE_OFFLINE = "OFFLINE";// 离线
    // 通道在线状态
    public static final String PLATFORM_DEVICE_CHANNEL_ONLINE = "ON";// 在线
    public static final String PLATFORM_DEVICE_CHANNEL_OFFLINE = "OFF";// 离线
}
