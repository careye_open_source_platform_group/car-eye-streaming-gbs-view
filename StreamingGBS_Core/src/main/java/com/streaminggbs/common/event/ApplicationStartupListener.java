package com.streaminggbs.common.event;

import com.streaminggbs.common.config.DeviceAlarmWebsocketConfig;
import com.streaminggbs.common.config.DeviceStatusWebsocketConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class ApplicationStartupListener {
    @Autowired
    DeviceAlarmWebsocketConfig deviceAlarmWebsocketConfig;
    @Autowired
    DeviceStatusWebsocketConfig deviceStatusWebsocketConfig;
    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.info("{}","初始化");
        try {
            deviceAlarmWebsocketConfig.startJob();
            deviceStatusWebsocketConfig.startJob();
        }catch (Exception e){
            log.error("",e);
        }
    }
}