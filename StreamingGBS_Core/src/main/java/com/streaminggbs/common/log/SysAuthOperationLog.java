package com.streaminggbs.common.log;

import java.lang.annotation.*;
/**
 * 定义操作日志注解
 * @author careye
 * @date 2022/09/20
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysAuthOperationLog {

    // 操作
    String operate();

}
