package com.streaminggbs.common.mongodb;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;


@ApiModel(value="设备返回的位置信息", description="设备返回的位置信息")
@Data
public class DeviceCoordinateModel implements Serializable{

	@ApiModelProperty(value = "设备ID")
	private String Id;

	@ApiModelProperty(value = "位置上报时间")
	private String Time;

	@ApiModelProperty(value = "返回的经度")
	private Double Longitude;

	@ApiModelProperty(value = "返回的纬度")
	private Double Latitude;

	@ApiModelProperty(value = "速度,单位:km/h")
	private Double Speed;

	@ApiModelProperty(value = "方向,取 值为当前摄像头方向与正北方的顺时针夹角,取值范围0°~360°,单位:(°)")
	private Double Direction;

	@ApiModelProperty(value = "海拔高度,单位:m")
	private Double Altitude;

	@ApiModelProperty(value = "创建时间")
	private Date CreateTime;
}
