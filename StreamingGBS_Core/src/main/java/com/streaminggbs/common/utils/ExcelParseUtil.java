package com.streaminggbs.common.utils;

import com.streaminggbs.entity.GbImport;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @类名称： @类描述：
 * 
 * @创建人：lilin
 * @创建时间：2020-10-19
 * @version 1.0
 */
@Slf4j
public class ExcelParseUtil {

    /**
     * excel文件转Workbook
     * @param filePath
     * @return
     * @throws Exception
     */
    private static Workbook getWorkbook(String filePath) throws Exception {
        String fileType = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length());
        InputStream stream = new FileInputStream(filePath);
        Workbook wb = null;
        if (fileType.equals("xls")) {
            wb = new HSSFWorkbook(stream);
        } else if (fileType.equals("xlsx")) {
            wb = new XSSFWorkbook(stream);
        } else {
            log.info("您输入的excel格式不正确");
        }
        return wb;
    }

    /**
     * 获取列值
     * @param row
     * @param i
     * @return
     */
    private static String getCellValue(Row row, int i) {
        Cell currentCell = row.getCell(i);
        if(currentCell != null){
            currentCell.setCellType(CellType.STRING);
            String data = currentCell.getStringCellValue();
            if(!StringUtil.isEmpty(data)){
                return data.trim();
            }
        }
        return null;
    }

    /**
     * 判断对象内的属性是否不全为空
     *
     * @param obj
     * @return
     * @throws IllegalAccessException
     */
    public static boolean checkObjFieldIsNull(Object obj) {
        boolean flag = false;
        try {
            for (Field f : obj.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                if (f.get(obj) != null) {
                    if(!Long.toString(1L).equals(f.get(obj).toString())){//排除掉版本号的属性值
                        flag = true;
                        return flag;
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 国标导入解析
     * @param filePath
     * @return
     */

    public static List<GbImport> GbExcelParse(String filePath){
        List<GbImport> list = new ArrayList<>();
        // 从文件流中获取Excel工作区对象（WorkBook）
        Workbook wb = null;
        try {
            wb = getWorkbook(filePath);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // 从工作区中取得页（Sheet）
        Sheet sheet = wb.getSheetAt(0);

        int n = 0;
        for (Row row : sheet) { // 循环打印Excel表中的内容
            n++;
            if (n < 2) {
                continue;
            }
            GbImport data = new GbImport();
            //国标编码
            String gbId = getCellValue(row,0);
            if(StringUtil.isEmpty(gbId)){
                continue;
            }
            data.setGbId(gbId);
            //父级国标编码
            String parentGbId = getCellValue(row,1);
            data.setParentGbId(parentGbId);
            //节点类型
            String type = getCellValue(row,2);
            if(!StringUtil.isEmpty(type)){
                data.setType(Integer.parseInt(type));
            }
            //节点名称
            String name = getCellValue(row,3);
            data.setName(name);
            //IP地址
            String ip = getCellValue(row,4);
            data.setIp(ip);
            //端口
            String port = getCellValue(row,5);
            data.setPort(port);
            //接入密码
            String password = getCellValue(row,6);
            data.setPort(port);
            //设备类型
            String devicetypename = getCellValue(row,7);
            data.setDevicetypename(devicetypename);

            list.add(data);
        }
        return list;

    }
}
