package com.streaminggbs.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @Author LiLin
 * @Date 2020/10/19 16:22
 * @Description 文件管理
 */
@Slf4j
public class FileHandler {

    /**
     * 上传文件根目录
     */
    public static final String BATH_PATH="./uploadfiles/";
    /**
     * 临时文件夹
     */
    public static final String TEMP_PATH="temp/";
    /**
     * 默认文件夹
     */
    public static final String DEFAULT_PATH="default/";

    /**
     * 保存文件
     * @param mFile
     * @return
     */
    public static String saveFile(MultipartFile mFile, String prePath) {

        String[] split = mFile.getOriginalFilename().split("\\.");
        String end = split[split.length-1];

        String fileName = IDManager.nextId() + "." + end;

        // 存贮文件
        BufferedOutputStream bof = null;
        String files = "";
        try {
            File file = new File(prePath);
            // 如果文件夹不存在则创建
            if (!file.exists() && !file.isDirectory()) {
                file.mkdirs();

                //设置可执行权限
                file.setExecutable(true, false);
                //设置可读权限
                file.setReadable(true, false);
                file.setWritable(true);

                if(file.getParentFile().exists()){
                    //设置可执行权限
                    file.getParentFile().setExecutable(true, false);
                    //设置可读权限
                    file.getParentFile().setReadable(true, false);
                    file.getParentFile().setWritable(true);
                }
            }
            files = prePath + fileName;
            log.info("文件路径：" + files);
            bof = new BufferedOutputStream(new FileOutputStream(new File(files)));
            bof.write(mFile.getBytes());
        } catch (IOException e) {
            fileName = null;
            log.error("存贮文件出错", e);
        } finally {
            if (bof != null) {
                try {
                    bof.flush();
                    bof.close();

                    File sfile = new File(files);
                    //设置可读权限
                    sfile.setReadable(true, false);
                    sfile.setWritable(true);
                } catch (IOException e) {
                    log.error("关闭文件流出错", e);
                }
            }
        }
        return fileName;
    }
}
