package com.streaminggbs.common.utils;


import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;


/*
 * 利用HttpClient进行post请求的工具类 
 */
@Slf4j
public class HttpClientUtil {
    /**
     * 发送get请求
     * @param url 请求路径
     * @return
     */
    public static String doGet(String url){
    	 String result = null;
    	 try{  

             URL url2 = new URL(url);
             HttpURLConnection conn = (HttpURLConnection)url2.openConnection();
             //设置超时间为3秒
             conn.setConnectTimeout(5*1000);
             conn.setReadTimeout(5*1000);
             //得到输入流
             InputStream inputStream;
             inputStream = conn.getInputStream();
             //获取自己数组
             return new String(readInputStream(inputStream),"utf-8");
	    } catch (Exception e) {
             log.error("",e);
	    }
	    return result;  
	}
    /**
     * 从输入流中获取字节数组
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static  byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        return bos.toByteArray();
    }
}  
