package com.streaminggbs.common.utils;

import com.streaminggbs.common.base.IDCreator;

/**
 * ID 生成器---->日志ID根据不同机器生成不同的
 * 
 * @author xuxiaoke
 *
 */
public class IDManager {
	public static int seqn = 0;
	// ID生成器
	public static IDCreator idCreator;

	public static String nextId() {
		if(idCreator == null){
			synchronized (IDManager.class) {
				if(idCreator == null){
					idCreator = new IDCreator();
				}
			}
		}
		return idCreator.nextId();
	}
	//获得发送序列号
	public static long getSerialId() {

		if (seqn > 2147483600) {
			seqn = 0;
		}
		seqn++;
		return seqn;
	}
}
