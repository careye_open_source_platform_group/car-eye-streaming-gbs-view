package com.streaminggbs.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @文件名称: Validator
 * @文件功能: 验证方式
 * @更改历史: 时间 用户 说明
 * @2020-10-19 lilin
 */
public class Validator {
    /**
     * 验证数字
     */
    private static Pattern patternNumber = Pattern.compile("^[0-9]+$");
    /**
     * 验证IP
     */
    private static Pattern patternIp = Pattern.compile("^((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)$");
    /**
     * 验证port
     */
    private static Pattern patternPort = Pattern.compile("^[1-9]\\d*$");

	
	/**
	 * 验证数字
	 * @param param
	 * @return
	 */
	public static boolean checkNumber(String param){
		if (param == null || param.equals("")) {
			return false;
		}
		Matcher matcher = patternNumber.matcher(param);
		return matcher.matches();
	}
	
	/**
	 * 验证IP
	 * @param param
	 * @return
	 */
	public static boolean checkIp(String param){
		if (param == null || param.equals("")) {
			return false;
		}
		Matcher matcher = patternIp.matcher(param);
		return matcher.matches();
	}

	/**
	 * 验证port
	 * @param param
	 * @return
	 */
	public static boolean checkPort(String param){
		if (param == null || param.equals("")) {
			return false;
		}
		Matcher matcher = patternPort.matcher(param);
		return matcher.matches() && Integer.parseInt(param) >= 1 && Integer.parseInt(param) <= 65535;
	}

	/**
	 * @功能说明 检测字符串是否为null或空格
	 * @param param
	 * @return boolean
	 */
	public static boolean checkIsNull(String param) {
		if (param == null || param.equals("")) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * @功能说明 检测字符串是否为数字，并且长度在 min 和 max 之间
	 * @method checkCharForNumber
	 * @param param
	 * @param min
	 * @param max
	 * @return boolean
	 */
	public static boolean checkCharForNumber(String param, int min, int max) {
		Pattern pattern = Pattern.compile("[0-9]{" + min + "," + max + "}");
		Matcher matcher = pattern.matcher(param);
		boolean b = matcher.matches();
		return b;
	}

    public static void main(String[] args) {

    }
}
