package com.streaminggbs.controller;

import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.common.base.ErrorCodeException;
import com.streaminggbs.common.base.ReturnBean;
import com.streaminggbs.common.config.StaticConfig;
import com.streaminggbs.common.constant.AlarmMethodEnum;
import com.streaminggbs.entity.AlarmInfo;
import com.streaminggbs.entity.LoginUser;
import com.streaminggbs.interfaces.DeviceAlarmService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @Author LiLin
 * @Date 2020/10/14 15:21
 * @Description 设备报警管理
 */
@Api(tags = {"设备管理接口"})
@Slf4j
@RestController
@RequestMapping("deviceAlarm")
public class DeviceAlarmController {

    @Autowired
    private DeviceAlarmService deviceAlarmService;

    @ApiOperation(value = "获取设备报警列表", notes = "获取设备报警列表")
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResponseEntity<ReturnBean> list(
            @ApiParam(value = "分页参数，从1开始") @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @ApiParam(value = "分页用") @RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
            BaseQueryBean baseQueryBean,
            @ModelAttribute LoginUser loginUser) {
        PageInfo<AlarmInfo> list = new PageInfo<>();
        try {
            baseQueryBean.setBelongDeptid(loginUser.getSysUser().getDeptid());

            list = deviceAlarmService.selectPageList(baseQueryBean, page, limit);
            list.getList().forEach(v -> {
                v.setAlarmMethodName(AlarmMethodEnum.getAlarmMethodByMethodType(v.getAlarmmethod()).getDesc());
            });
        } catch (Exception e) {
            log.error("获取当前的设备报警异常!{}",e);
        }
        return new ResponseEntity<>(new ReturnBean(list), HttpStatus.OK);
    }

    @ApiOperation(value = "删除设备报警", notes = "删除设备报警")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ReturnBean delete(
            @ApiParam(value = "设备ID") @RequestParam(value = "id", required = true) String id
    ) {
        log.info("======删除设备报警======>id:" + id);
        try {
            deviceAlarmService.delete(id);

            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==设备报警管理==删除设备报警报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "删除全部设备报警", notes = "删除全部设备报警")
    @RequestMapping(value = "/deleteAll", method = RequestMethod.POST)
    public ReturnBean deleteAll(
    ) {
        log.info("======删除全部设备报警======>");
        try {
            deviceAlarmService.deleteAll();

            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==设备报警管理==删除设备报警报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }
}
