package com.streaminggbs.controller;

import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.common.base.ErrorCodeException;
import com.streaminggbs.common.base.ReturnBean;
import com.streaminggbs.common.config.StaticConfig;
import com.streaminggbs.entity.DeviceChannelInfo;
import com.streaminggbs.entity.DeviceInfo;
import com.streaminggbs.interfaces.DeviceChannelInfoService;
import com.streaminggbs.common.utils.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author LiLin
 * @Date 2020/9/12 9:23
 * @Description 通道管理
 */
@Api(tags = {"通道管理接口"})
@Slf4j
@RestController
@RequestMapping("deviceChannelInfo")
public class DeviceChannelController {

    @Autowired
    private DeviceChannelInfoService deviceChannelInfoService;
    @Autowired
    private RedisUtil redisUtil;

    @ApiOperation(value = "获取通道信息列表", notes = "获取通道信息列表")
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResponseEntity<ReturnBean> list(
            @ApiParam(value = "分页参数，从1开始") @RequestParam(value = "page", required = false) Integer page,
            @ApiParam(value = "分页用") @RequestParam(value = "limit", required = false) Integer limit,
            BaseQueryBean baseQueryBean) {

        PageInfo<DeviceChannelInfo> list = new PageInfo<>();
        try {
            if (page == null || limit == null) {
                list = deviceChannelInfoService.selectList(baseQueryBean);
            } else{
                list = deviceChannelInfoService.selectPageList(baseQueryBean, page, limit);
            }
        } catch (Exception e) {
            log.error("获取当前的通道信息异常!{}", e);
        }
        return new ResponseEntity<>(new ReturnBean(list), HttpStatus.OK);
    }

    @ApiOperation(value = "添加通道", notes = "添加通道")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ReturnBean add(
            @ApiParam(value = "设备ID") @RequestParam(value = "deviceid", required = true) String deviceid,
            @ApiParam(value = "通道编号") @RequestParam(value = "channel", required = true) String channel,
            @ApiParam(value = "通道名") @RequestParam(value = "channelname", required = false) String channelname,
            @ApiParam(value = "是否支持云台（0不支持  1支持）") @RequestParam(value = "ptzEnable", required = true) Integer ptzEnable,
            @ApiParam(value = "是否支持对讲（0不支持  1支持）") @RequestParam(value = "talkEnbale", required = true) Integer talkEnbale,
            DeviceChannelInfo entity
    ) {
        log.info("======添加通道======>deviceid:" + deviceid + "，channel:" + channel + "，channelname:" + channelname
                + "，ptzEnable:" + ptzEnable + "，talkEnbale:" + talkEnbale);
        try {

            int count = deviceChannelInfoService.addChannelInfo(entity);
            if (count == -1) {
                return new ReturnBean(-1, "通道编码已存在");
            }

            return new ReturnBean("保存成功");
        } catch (Exception e) {
            log.error("==通道管理==添加通道报错==", e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "编辑通道", notes = "编辑通道")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ReturnBean edit(
            @ApiParam(value = "通道ID") @RequestParam(value = "id", required = true) String id,
            @ApiParam(value = "设备ID") @RequestParam(value = "deviceid", required = true) String deviceid,
            @ApiParam(value = "通道编号") @RequestParam(value = "channel", required = true) String channel,
            @ApiParam(value = "通道名") @RequestParam(value = "channelname", required = false) String channelname,
            @ApiParam(value = "是否支持云台（0不支持  1支持）") @RequestParam(value = "ptzEnable", required = true) Integer ptzEnable,
            @ApiParam(value = "是否支持对讲（0不支持  1支持）") @RequestParam(value = "talkEnbale", required = true) Integer talkEnbale,
            @ApiParam(value = "通道类型") @RequestParam(value = "type", required = true) Integer type,
            DeviceChannelInfo entity
    ) {
        log.info("======编辑通道======>id:" + id + "，deviceid:" + deviceid + "，channel:" + channel + "，channelname:" + channelname
                + "，ptzEnable:" + ptzEnable + "，talkEnbale:" + talkEnbale + "，type:" + type);
        try {

            int count = deviceChannelInfoService.editChannelInfo(entity);
            if (count == -1) {
                return new ReturnBean(-1, "通道编码已存在");
            }

            return new ReturnBean("保存成功");
        } catch (Exception e) {
            log.error("==通道管理==编辑通道报错==", e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "删除通道", notes = "删除通道")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ReturnBean delete(
            @ApiParam(value = "通道ID") @RequestParam(value = "id", required = true) String id
    ) {
        log.info("======删除通道======>id:" + id);
        try {
            deviceChannelInfoService.deleteChannelInfo(id);

            return new ReturnBean("保存成功");
        } catch (Exception e) {
            log.error("==通道管理==删除通道报错==", e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }
}
