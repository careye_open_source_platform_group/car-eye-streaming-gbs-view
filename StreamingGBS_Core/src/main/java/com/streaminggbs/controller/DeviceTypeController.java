package com.streaminggbs.controller;

import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.common.base.ErrorCodeException;
import com.streaminggbs.common.base.ReturnBean;
import com.streaminggbs.common.config.StaticConfig;
import com.streaminggbs.entity.SettingsDeviceType;
import com.streaminggbs.interfaces.SettingsDeviceTypeService;
import com.streaminggbs.common.utils.IDManager;
import com.streaminggbs.common.utils.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author LiLin
 * @Date 2020/9/15 9:23
 * @Description 设备类型管理
 */
@Api(tags = {"设备类型管理接口"})
@Slf4j
@RestController
@RequestMapping("deviceType")
public class DeviceTypeController {

    @Autowired
    private SettingsDeviceTypeService settingsDeviceTypeService;
    @Autowired
    private RedisUtil redisUtil;

    @ApiOperation(value = "获取设备类型信息列表", notes = "获取设备类型信息列表")
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResponseEntity<ReturnBean> list(
            @ApiParam(value = "分页参数，从1开始") @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @ApiParam(value = "分页用") @RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
            BaseQueryBean baseQueryBean) {

        PageInfo<SettingsDeviceType> list = new PageInfo<>();
        try {
            list = settingsDeviceTypeService.selectPageList(baseQueryBean, page, limit);
        } catch (Exception e) {
            log.error("获取当前的设备类型信息异常!{}", e);
        }
        return new ResponseEntity<>(new ReturnBean(list), HttpStatus.OK);
    }

    @ApiOperation(value = "添加设备类型", notes = "添加设备类型")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ReturnBean add(
            @ApiParam(value = "协议类型") @RequestParam(value = "protocol", required = true) Integer protocol,
            @ApiParam(value = "设备类型名称") @RequestParam(value = "typename", required = true) String typename,
            SettingsDeviceType entity
    ) {
        log.info("======添加设备类型======>protocol:" + protocol + "，typename:" + typename);
        try {
            //验证设备类型是否已存在
            SettingsDeviceType paramInfo = new SettingsDeviceType();
            paramInfo.setTypename(typename);
            SettingsDeviceType oldInfo = settingsDeviceTypeService.selectByOther(paramInfo);
            if (oldInfo != null) {
                return new ReturnBean(-1, "设备类型名称已存在");
            }

            entity.setId(IDManager.nextId());
            settingsDeviceTypeService.save(entity);
            //redisUtil.hset(RedisUtil.REDIS_DEVICE_TYPE, entity.getId(), entity);
            return new ReturnBean("保存成功");
        } catch (Exception e) {
            log.error("==设备类型管理==添加设备类型报错==", e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "编辑设备类型", notes = "编辑设备类型")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ReturnBean edit(
            @ApiParam(value = "设备类型ID") @RequestParam(value = "id", required = true) String id,
            @ApiParam(value = "协议类型") @RequestParam(value = "protocol", required = true) Integer protocol,
            @ApiParam(value = "设备类型名称") @RequestParam(value = "typename", required = true) String typename,
            SettingsDeviceType entity
    ) {
        log.info("======编辑设备类型======>id:" + id + "，protocol:" + protocol + "，typename:" + typename);
        try {
            //验证设备类型是否已存在
            SettingsDeviceType paramInfo = new SettingsDeviceType();
            paramInfo.setTypename(typename);
            SettingsDeviceType oldInfo = settingsDeviceTypeService.selectByOther(paramInfo);
            if (oldInfo != null && !id.equals(oldInfo.getId())) {
                return new ReturnBean(-1, "设备类型名称已存在");
            }

            settingsDeviceTypeService.updateNotNull(entity);
            //redisUtil.hset(RedisUtil.REDIS_DEVICE_TYPE, entity.getId(), entity);
            return new ReturnBean("保存成功");
        } catch (Exception e) {
            log.error("==设备类型管理==编辑设备类型报错==", e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "删除设备类型", notes = "删除设备类型")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ReturnBean delete(
            @ApiParam(value = "设备类型ID") @RequestParam(value = "id", required = true) String id
    ) {
        log.info("======删除设备类型======>id:" + id);
        try {
            settingsDeviceTypeService.delete(id);
            //redisUtil.hdel(RedisUtil.REDIS_DEVICE_TYPE, id);
            return new ReturnBean("保存成功");
        } catch (Exception e) {
            log.error("==设备类型管理==删除设备类型报错==", e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "获取下拉设备类型列表", notes = "获取下拉设备类型列表")
    @RequestMapping(value = "/getSelectDeviceTypeList", method = RequestMethod.GET)
    public ReturnBean getSelectRoleList(
    ) {
        try {
            return new ReturnBean(settingsDeviceTypeService.selectAll());
        } catch (Exception e) {
            log.error("==设备类型管理==获取下拉设备类型列表报错==", e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }
}
