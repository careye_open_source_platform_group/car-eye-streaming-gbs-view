package com.streaminggbs.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.streaminggbs.common.base.ErrorCodeException;
import com.streaminggbs.common.base.ReturnBean;
import com.streaminggbs.common.config.StaticConfig;
import com.streaminggbs.entity.NodeMenu;
import com.streaminggbs.entity.SysAuthUser;
import com.streaminggbs.entity.SysParamSet;
import com.streaminggbs.interfaces.SysAuthLoginLogService;
import com.streaminggbs.interfaces.SysAuthMenuService;
import com.streaminggbs.interfaces.SysAuthUserService;
import com.streaminggbs.interfaces.SysParamSetService;
import com.streaminggbs.common.utils.DateUtil;
import com.streaminggbs.common.utils.IpUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author LiLin
 * @Date 2020/9/16 14:38
 * @Description 登录接口
 */
@Api(tags = {"登录/登出接口"})
@Slf4j
@RestController
public class LoginController {

    @Autowired
    private SysAuthUserService sysAuthUserService;
    @Autowired
    private SysAuthMenuService sysAuthMenuService;
    @Autowired
    private SysAuthLoginLogService sysAuthLoginLogService;
    @Autowired
    private SysParamSetService sysParamSetService;

    @Value("${spring.session.timeout}")
    private Duration defaultSessionTimeout;

    @ApiOperation(value = "用户登录", notes = "用户登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ReturnBean login(HttpServletRequest request, HttpServletResponse response,
                            @ApiParam(value = "登录名") @RequestParam(value = "loginname", required = true) String loginname,
                            @ApiParam(value = "密码") @RequestParam(value = "password", required = true) String password,
                            @ApiParam(value = "验证码") @RequestParam(value = "checkCode", required = true) String checkCode
    ) {
        log.info("======登录======>loginname:" + loginname + "，password:" + password);
        try {
            String cscode = ((String) request.getSession().getAttribute("checkCode"));
            if (null == cscode) {
                return new ReturnBean(-2, "验证码错误");
            }
            cscode = cscode.toLowerCase();//获取图片上验证码的值
            if(checkCode!=null) {
                checkCode = checkCode.toLowerCase();
            }
            if(null!=cscode && !"".equals(cscode)&&cscode.equals(checkCode)) {
                //验证用户是否已存在
                SysAuthUser paramInfo = new SysAuthUser();
                paramInfo.setLoginname(loginname);
                paramInfo.setPassword(password);
                SysAuthUser oldInfo = sysAuthUserService.selectByOther(paramInfo);
                if (oldInfo == null) {
                    return new ReturnBean(-1, "用户名或密码错误");
                }
                //验证用户通过后，将用户相关信息存入session
                request.getSession().setAttribute("user", oldInfo);
                //查询有权限的菜单
                String usergroupid = oldInfo.getUsergroupid();
                if ("admin".equals(oldInfo.getLoginname())) {
                    usergroupid = null;
                }
                List<NodeMenu> menuList = sysAuthMenuService.getAuthMenuList(usergroupid);
                request.getSession().setAttribute("menus", menuList);
                //查询有权限的按钮
                List<String> buttonList = sysAuthMenuService.getAuthButtonList(usergroupid);
                request.getSession().setAttribute("buttons", buttonList);

                request.getSession().setMaxInactiveInterval(getSessionTimeout());

                //登录成功更新登录时间
                SysAuthUser updateUser = new SysAuthUser();
                updateUser.setUserid(oldInfo.getUserid());
                updateUser.setLogintime(DateUtil.getDate());
                sysAuthUserService.updateNotNull(updateUser);

                //添加登录日志
                sysAuthLoginLogService.addLoginLog(oldInfo.getDeptid(),
                        oldInfo.getUserid(), IpUtil.getRemoteHost(request), 1);

                return new ReturnBean("登录成功");
            }else{
                return new ReturnBean(-2,"验证码错误");
            }
        }catch (Exception e){
            log.error("==登录接口==登录报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "保持连接", notes = "保持连接")
    @RequestMapping(value = "/keep", method = {RequestMethod.GET,RequestMethod.POST})
    public ReturnBean keep1(HttpServletRequest request, HttpServletResponse response) {
        try {
            log.info("保持连接1");
            JSONObject json = new JSONObject();
            json.put("timeout", getSessionTimeout());
            return new ReturnBean(json);
        } catch (Exception e) {
            log.error("==保持连接接口==登出报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    private int getSessionTimeout() {
    	SysParamSet sysParam = sysParamSetService.pick();
        Integer sessionTimeout = null == sysParam ? null : sysParam.getSessionTimeout();
        if (sessionTimeout == null || sessionTimeout <= 0) {
        	sessionTimeout = (int) defaultSessionTimeout.toMinutes();
        }
        return sessionTimeout *= 60;
    }

    @ApiOperation(value = "用户登出", notes = "用户登出")
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ReturnBean logout(HttpServletRequest request, HttpServletResponse response
    ) {
        try {
            SysAuthUser user = (SysAuthUser) request.getSession().getAttribute("user");
            log.info("======登出======>userid:" + user.getUserid());
            request.getSession().invalidate();
            //登出成功更新登出时间
            SysAuthUser updateUser = new SysAuthUser();
            updateUser.setUserid(user.getUserid());
            updateUser.setExittime(DateUtil.getDate());
            sysAuthUserService.updateNotNull(updateUser);
            //添加登出日志
            sysAuthLoginLogService.addLoginLog(user.getDeptid(),
                    user.getUserid(), IpUtil.getRemoteHost(request), 2);
            return new ReturnBean("登出成功");
        }catch (Exception e){
            log.error("==登录接口==登出报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "获取验证码", notes = "获取验证码")
    @RequestMapping(value = "/check", method = {RequestMethod.GET,RequestMethod.POST})
    public void check(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 响应头信息
        response.setHeader("Pragma", "No-Cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expries", 0);
        response.setContentType("image/jpeg");

        // 随机数生成类
        Random random = new Random();

        // 定义验证码的位数
        int size = 5;

        // 定义变量保存生成的验证码
        String vCode = "";
        char c;
        // 产生验证码
        for (int i = 0; i < size; i++) {
            // 产生一个26以内的随机整数
            int number = random.nextInt(26);
            // 如果生成的是偶数，则随机生成一个数字
            if (number % 2 == 0) {
                c = (char) ('0' + (char) ((int) (Math.random() * 10)));
                // 如果生成的是奇数，则随机生成一个字母
            } else {
                c = (char) ((char) ((int) (Math.random() * 26)) + 'A');
            }
            vCode = vCode + c;
        }

        log.info("获取的验证码:{}",vCode.toLowerCase());
        // 保存生成的5位验证码
        request.getSession().setAttribute("checkCode", vCode.toLowerCase());

        // 验证码图片的生成
        // 定义图片的宽度和高度
        int width = (int) Math.ceil(size * 20);
        int height = 34;
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // 获取图片的上下文
        Graphics gr = image.getGraphics();
        // 设定图片背景颜色
        gr.setColor(Color.WHITE);
        gr.fillRect(0, 0, width, height);
        // 设定图片边框
        gr.setColor(Color.GRAY);
        gr.drawRect(0, 0, width - 1, height - 1);
        // 画十条干扰线
        for (int i = 0; i < 5; i++) {
            int x1 = random.nextInt(width);
            int y1 = random.nextInt(height);
            int x2 = random.nextInt(width);
            int y2 = random.nextInt(height);
            gr.setColor(randomColor());
            gr.drawLine(x1, y1, x2, y2);
        }
        // 设置字体，画验证码
        gr.setColor(randomColor());
        gr.setFont(randomFont());
        gr.drawString(vCode, 10, 22);
        // 图像生效
        gr.dispose();
        // 输出到页面
        ImageIO.write(image, "JPEG", response.getOutputStream());
    }

    // 生成随机的颜色
    private Color randomColor() {
        int red = r.nextInt(150);
        int green = r.nextInt(150);
        int blue = r.nextInt(150);
        return new Color(red, green, blue);
    }

    private String[] fontNames = { "Times New Roman" };
    private Random r = new Random();

    // 生成随机的字体
    private Font randomFont() {
        int index = r.nextInt(fontNames.length);
        String fontName = fontNames[index];// 生成随机的字体名称
        int style = r.nextInt(1);
        int size = r.nextInt(3) + 24; // 生成随机字号, 24 ~ 28
        return new Font(fontName, style, size);
    }
}
