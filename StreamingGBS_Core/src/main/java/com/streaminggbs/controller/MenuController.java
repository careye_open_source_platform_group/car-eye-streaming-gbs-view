package com.streaminggbs.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.common.base.ErrorCodeException;
import com.streaminggbs.common.base.ReturnBean;
import com.streaminggbs.common.config.StaticConfig;
import com.streaminggbs.entity.SysAuthMenu;
import com.streaminggbs.interfaces.SysAuthMenuService;
import com.streaminggbs.common.utils.IDManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author LiLin
 * @Date 2020/9/15 15:23
 * @Description 菜单管理
 */
@Api(tags = {"菜单管理接口"})
@Slf4j
@RestController
@RequestMapping("menu")
public class MenuController {

    @Autowired
    private SysAuthMenuService sysAuthMenuService;

    @ApiOperation(value = "获取菜单信息列表", notes = "获取菜单信息列表")
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResponseEntity<ReturnBean> list(
            @ApiParam(value = "分页参数，从1开始") @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @ApiParam(value = "分页用") @RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
            BaseQueryBean baseQueryBean) {

        PageInfo<SysAuthMenu> list = new PageInfo<>();
        try {
            list = sysAuthMenuService.selectPageList(baseQueryBean, page, limit);
        } catch (Exception e) {
            log.error("获取当前的菜单信息异常!{}",e);
        }
        return new ResponseEntity<>(new ReturnBean(list), HttpStatus.OK);
    }

    @ApiOperation(value = "添加菜单", notes = "添加菜单")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ReturnBean add(
            @ApiParam(value = "父级菜单ID") @RequestParam(value = "parentmenuid", required = true) String parentmenuid,
            @ApiParam(value = "菜单名称") @RequestParam(value = "menuname", required = true) String menuname,
            @ApiParam(value = "菜单地址") @RequestParam(value = "menuaddr", required = false) String menuaddr,
            @ApiParam(value = "是否显示(0不显示 1显示)") @RequestParam(value = "displaytype", required = false) Integer displaytype,
            @ApiParam(value = "排序号") @RequestParam(value = "menusort", required = false) Integer menusort,
            @ApiParam(value = "菜单级别(1一级， 2 二级）") @RequestParam(value = "menulevel", required = false) String menulevel,
            @ApiParam(value = "菜单类型(0-菜单  1-按钮)") @RequestParam(value = "menutype", required = false) String menutype,
            @ApiParam(value = "图片地址（font-awesome）") @RequestParam(value = "imgurl", required = false) String imgurl,
            SysAuthMenu entity
    ) {
        log.info("======添加菜单======>parentmenuid:" + parentmenuid + "，menuname:" + menuname + "，menuaddr:" + menuaddr
                + "，displaytype:" + displaytype + "，menusort:" + menusort + "，menulevel:" + menulevel
                + "，menutype:" + menutype + "，imgurl:" + imgurl);
        try {
            //验证菜单是否已存在
            SysAuthMenu paramInfo = new SysAuthMenu();
            paramInfo.setParentmenuid(parentmenuid);
            paramInfo.setMenuname(menuname);
            SysAuthMenu oldInfo = sysAuthMenuService.selectByOther(paramInfo);
            if(oldInfo != null){
                return new ReturnBean(-1, "菜单名称已存在");
            }

            entity.setMenuid(IDManager.nextId());
            sysAuthMenuService.save(entity);

            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==菜单管理==添加菜单报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "编辑菜单", notes = "编辑菜单")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ReturnBean edit(
            @ApiParam(value = "菜单ID") @RequestParam(value = "menuid", required = true) String menuid,
            @ApiParam(value = "父级菜单ID") @RequestParam(value = "parentmenuid", required = true) String parentmenuid,
            @ApiParam(value = "菜单名称") @RequestParam(value = "menuname", required = true) String menuname,
            @ApiParam(value = "菜单地址") @RequestParam(value = "menuaddr", required = false) String menuaddr,
            @ApiParam(value = "是否显示(0不显示 1显示)") @RequestParam(value = "displaytype", required = false) Integer displaytype,
            @ApiParam(value = "排序号") @RequestParam(value = "menusort", required = false) Integer menusort,
            @ApiParam(value = "菜单级别(1一级， 2 二级）") @RequestParam(value = "menulevel", required = false) String menulevel,
            @ApiParam(value = "菜单类型(0-菜单  1-按钮)") @RequestParam(value = "menutype", required = false) String menutype,
            @ApiParam(value = "图片地址（font-awesome）") @RequestParam(value = "imgurl", required = false) String imgurl,
            SysAuthMenu entity
    ) {
        log.info("======编辑菜单======>menuid:" + menuid + "，parentmenuid:" + parentmenuid + "，menuname:" + menuname + "，menuaddr:" + menuaddr
                + "，displaytype:" + displaytype + "，menusort:" + menusort + "，menulevel:" + menulevel
                + "，menutype:" + menutype + "，imgurl:" + imgurl);
        try {
            //验证菜单是否已存在
            SysAuthMenu paramInfo = new SysAuthMenu();
            paramInfo.setParentmenuid(parentmenuid);
            paramInfo.setMenuname(menuname);
            SysAuthMenu oldInfo = sysAuthMenuService.selectByOther(paramInfo);
            if(oldInfo != null && !menuid.equals(oldInfo.getMenuid())){
                return new ReturnBean(-1, "菜单名称已存在");
            }

            sysAuthMenuService.updateNotNull(entity);

            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==菜单管理==编辑菜单报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "删除菜单", notes = "删除菜单")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ReturnBean delete(
            @ApiParam(value = "菜单ID") @RequestParam(value = "menuid", required = true) String menuid
    ) {
        log.info("======删除菜单======>menuid:" + menuid);
        try {
            sysAuthMenuService.delete(menuid);

            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==菜单管理==删除菜单报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "获取下拉菜单列表", notes = "获取下拉菜单列表")
    @RequestMapping(value = "/getSelectMenuList", method = RequestMethod.GET)
    public ReturnBean getSelectRoleList(
            @ApiParam(value = "菜单级别(1一级， 2 二级）") @RequestParam(value = "menulevel", required = true) Integer menulevel
    ) {
        try {
            JSONArray arr = new JSONArray();
            if(menulevel <= 0){
                JSONObject tempObj = new JSONObject();
                tempObj.put("menuid", "-1");
                tempObj.put("menuname", "无");
                tempObj.put("parentid", "");
                tempObj.put("children", new ArrayList<>());
                arr.add(tempObj);
                return new ReturnBean(arr);
            }else{
                Example example = new Example(SysAuthMenu.class);
                Example.Criteria criteria = example.createCriteria();
                criteria.andEqualTo("menulevel", menulevel);
                example.orderBy("menuid").desc();
                List<SysAuthMenu> parentMenuList =  sysAuthMenuService.selectByExample(example);
                if(parentMenuList != null && !parentMenuList.isEmpty()){
                    for (SysAuthMenu menu : parentMenuList) {
                        JSONObject tempObj = new JSONObject();
                        tempObj.put("menuid", menu.getMenuid());
                        tempObj.put("menuname", menu.getMenuname());
                        tempObj.put("parentid", "");
                        tempObj.put("children", new ArrayList<>());
                        arr.add(tempObj);
                    }
                    return new ReturnBean(arr);
                }else{
                    return new ReturnBean(-1, "无数据");
                }
            }
        }catch (Exception e){
            log.error("==菜单管理==获取下拉菜单列表报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }
}
