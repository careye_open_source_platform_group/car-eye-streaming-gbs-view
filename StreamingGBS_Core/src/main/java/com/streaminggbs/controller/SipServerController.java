package com.streaminggbs.controller;

import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.common.base.ErrorCodeException;
import com.streaminggbs.common.base.ReturnBean;
import com.streaminggbs.common.config.StaticConfig;
import com.streaminggbs.entity.SettingSipServerInfo;
import com.streaminggbs.interfaces.Gb28181Service;
import com.streaminggbs.interfaces.SettingSipServerInfoService;
import com.streaminggbs.common.utils.IDManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lilin
 * @date 2020-09-22 19:40
 * @discription SIP上级服务器管理
 */
@Api(tags = {"上级服务器管理接口"})
@Slf4j
@RestController
@RequestMapping("sipServer")
public class SipServerController {

    @Autowired
    private SettingSipServerInfoService settingSipServerInfoService;

    @ApiOperation(value = "获取上级服务器列表", notes = "获取上级服务器列表")
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResponseEntity<ReturnBean> list(
            @ApiParam(value = "分页参数，从1开始") @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @ApiParam(value = "分页用") @RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
            BaseQueryBean baseQueryBean) {
        PageInfo<SettingSipServerInfo> list = new PageInfo<>();
        try {
            list = settingSipServerInfoService.selectPageList(baseQueryBean, page, limit);
        } catch (Exception e) {
            log.error("获取当前的上级服务器异常!{}",e);
        }
        return new ResponseEntity<>(new ReturnBean(list), HttpStatus.OK);
    }

    @ApiOperation(value = "添加上级服务器", notes = "添加上级服务器")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ReturnBean add(
            @ApiParam(value = "级联服务器名") @RequestParam(value = "name", required = true) String name,
            @ApiParam(value = "国标编码") @RequestParam(value = "code", required = true) String code,
            @ApiParam(value = "国标服务器域") @RequestParam(value = "domain", required = false) String domain,
            @ApiParam(value = "国标服务器ip") @RequestParam(value = "ip", required = false) String ip,
            @ApiParam(value = "端口") @RequestParam(value = "port", required = false) String port,
            @ApiParam(value = "用户名") @RequestParam(value = "user", required = false) String user,
            @ApiParam(value = "密码") @RequestParam(value = "password", required = false) String password,
            @ApiParam(value = "注册周期(秒)") @RequestParam(value = "registerPeroid", required = false) Integer registerPeroid,
            @ApiParam(value = "心跳周期(秒)") @RequestParam(value = "heartPeroid", required = false) Integer heartPeroid,
            @ApiParam(value = "目录分组大小") @RequestParam(value = "gourpSize", required = false) Integer gourpSize,
            @ApiParam(value = "传输协议(0：UDP 1：tcp)") @RequestParam(value = "transProtocol", required = false) Integer transProtocol,
            @ApiParam(value = "字符集(0：GB2312 1：UTF-8)") @RequestParam(value = "codeSet", required = false) Integer codeSet,
            @ApiParam(value = "是否启用(0：未启用 1：启用)") @RequestParam(value = "active", required = false) Integer active,
            @ApiParam(value = "是否启用RTCP(0：未启用 1：启动)") @RequestParam(value = "rtcp", required = false) Integer rtcp,
            SettingSipServerInfo entity
    ) {
        log.info("======添加上级服务器======>name:" + name + "，code:" + code + "，domain:" + domain
                + "，ip:" + ip + "，port:" + port + "，user:" + user + "，password:" + password + "，registerPeroid:" + registerPeroid
                + "，heartPeroid:" + heartPeroid + "，gourpSize:" + gourpSize + "，transProtocol:" + transProtocol + "，codeSet:" + codeSet
                + "，active:" + active + "，rtcp:" + rtcp);
        try {
            //验证上级服务器是否已存在
            SettingSipServerInfo paramInfo = new SettingSipServerInfo();
            paramInfo.setCode(code);
            SettingSipServerInfo oldInfo = settingSipServerInfoService.selectByOther(paramInfo);
            if(oldInfo != null){
                return new ReturnBean(-1, "国标编码已存在");
            }

            entity.setId(IDManager.nextId());
            settingSipServerInfoService.save(entity);
            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==上级服务器管理==添加上级服务器报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "编辑上级服务器", notes = "编辑上级服务器")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ReturnBean edit(
            @ApiParam(value = "上级服务器ID") @RequestParam(value = "id", required = true) String id,
            @ApiParam(value = "级联服务器名") @RequestParam(value = "name", required = true) String name,
            @ApiParam(value = "国标编码") @RequestParam(value = "code", required = true) String code,
            @ApiParam(value = "国标服务器域") @RequestParam(value = "domain", required = false) String domain,
            @ApiParam(value = "国标服务器ip") @RequestParam(value = "ip", required = false) String ip,
            @ApiParam(value = "端口") @RequestParam(value = "port", required = false) String port,
            @ApiParam(value = "用户名") @RequestParam(value = "user", required = false) String user,
            @ApiParam(value = "密码") @RequestParam(value = "password", required = false) String password,
            @ApiParam(value = "注册周期(秒)") @RequestParam(value = "registerPeroid", required = false) Integer registerPeroid,
            @ApiParam(value = "心跳周期(秒)") @RequestParam(value = "heartPeroid", required = false) Integer heartPeroid,
            @ApiParam(value = "目录分组大小") @RequestParam(value = "gourpSize", required = false) Integer gourpSize,
            @ApiParam(value = "传输协议(0：UDP 1：tcp)") @RequestParam(value = "transProtocol", required = false) Integer transProtocol,
            @ApiParam(value = "字符集(0：GB2312 1：UTF-8)") @RequestParam(value = "codeSet", required = false) Integer codeSet,
            @ApiParam(value = "是否启用(0：未启用 1：启用)") @RequestParam(value = "active", required = false) Integer active,
            @ApiParam(value = "是否启用RTCP(0：未启用 1：启动)") @RequestParam(value = "rtcp", required = false) Integer rtcp,
            SettingSipServerInfo entity
    ) {
        log.info("======编辑上级服务器======>id:" + id + "，name:" + name + "，code:" + code + "，domain:" + domain
                + "，ip:" + ip + "，port:" + port + "，user:" + user + "，password:" + password + "，registerPeroid:" + registerPeroid
                + "，heartPeroid:" + heartPeroid + "，gourpSize:" + gourpSize + "，transProtocol:" + transProtocol + "，codeSet:" + codeSet
                + "，active:" + active + "，rtcp:" + rtcp);
        try {
            //验证上级服务器是否已存在
            SettingSipServerInfo paramInfo = new SettingSipServerInfo();
            paramInfo.setCode(code);
            SettingSipServerInfo oldInfo = settingSipServerInfoService.selectByOther(paramInfo);
            if(oldInfo != null && !id.equals(oldInfo.getId())){
                return new ReturnBean(-1, "国标编码已存在");
            }

            settingSipServerInfoService.updateNotNull(entity);
            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==上级服务器管理==编辑上级服务器报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "删除上级服务器", notes = "删除上级服务器")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ReturnBean delete(
            @ApiParam(value = "上级服务器ID") @RequestParam(value = "id", required = true) String id
    ) {
        log.info("======删除上级服务器======>id:" + id);
        try {
            settingSipServerInfoService.delete(id);
            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==上级服务器管理==删除上级服务器报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }
}
