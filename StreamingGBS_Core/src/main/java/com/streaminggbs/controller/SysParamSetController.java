package com.streaminggbs.controller;

import java.time.Duration;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.streaminggbs.common.base.ErrorCodeException;
import com.streaminggbs.common.base.ReturnBean;
import com.streaminggbs.common.config.StaticConfig;
import com.streaminggbs.entity.SysParamSet;
import com.streaminggbs.interfaces.SysParamSetService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author LiLin
 * @Date 2020/10/10 15:24
 * @Description 系统参数设置
 */
@Api(tags = { "参数设置接口" })
@Slf4j
@RestController
@RequestMapping("sysParamSet")
public class SysParamSetController {

	@Value("${spring.session.timeout}")
	private Duration defaultSessionTimeout;

	@Autowired
	private SysParamSetService sysParamSetService;

	@ApiOperation(value = "获取参数信息", notes = "获取参数信息")
	@GetMapping("/getParamInfo")
	public ReturnBean getParamInfo() {
		return new ReturnBean(sysParamSetService.pick());
	}

	@ApiOperation(value = "保存参数信息", notes = "保存参数信息")
	@PostMapping("/save")
	public ReturnBean save(
		HttpServletRequest request,
		@ApiParam(value = "视频关闭延迟") @RequestParam(required = false) Integer closeDelay,
		@ApiParam(value = "历史视频清理周期") @RequestParam(required = false) Integer clearVideoPeriod,
		@ApiParam(value = "登陆超时") @RequestParam(required = false) Integer sessionTimeout,
		@ApiParam(value = "视频服务器地址") @RequestParam(required = false) String videoServerUrl
	) {
		final String id = "1";
		SysParamSet entity = new SysParamSet();
		entity.setId(id);
		entity.setCloseDelay(closeDelay);
		entity.setSessionTimeout(sessionTimeout);
		entity.setVideoServerUrl(videoServerUrl);
		try {
			// 查询id=1的记录是否存在
			SysParamSet oldInfo = sysParamSetService.selectByKey(id);
			if (oldInfo == null) {
				// 当查询不到记录的时候，先删除所有记录，然后插入id=1的记录
				sysParamSetService.deleteAll();
				sysParamSetService.save(entity);
			} else {
				sysParamSetService.updateNotNull(entity);
			}
			try {
				SysParamSet sysParamSet = sysParamSetService.pick();
				Integer sessionTimeOut = sysParamSet.getSessionTimeout();
				if (sessionTimeout == null || sessionTimeout <= 0) {
		        	sessionTimeout = (int) defaultSessionTimeout.toMinutes();
		        }
				sessionTimeOut *= 60;
				request.getSession().setMaxInactiveInterval(sessionTimeOut);
			} catch (Exception e) {
				log.error("保存参数信息失败", e);
			}
			return new ReturnBean(true);
		} catch (Exception e) {
			log.error("保存参数信息失败", e);
			throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
		}
	}
}
