package com.streaminggbs.controller;

import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.common.base.ErrorCodeException;
import com.streaminggbs.common.base.ReturnBean;
import com.streaminggbs.common.config.StaticConfig;
import com.streaminggbs.entity.SysAuthUserGroup;
import com.streaminggbs.interfaces.SysAuthUserGroupService;
import com.streaminggbs.common.utils.IDManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author LiLin
 * @Date 2020/9/14 9:23
 * @Description 角色管理
 */
@Api(tags = {"角色管理接口"})
@Slf4j
@RestController
@RequestMapping("userGroup")
public class UserGroupController {

    @Autowired
    private SysAuthUserGroupService sysAuthUserGroupService;

    @ApiOperation(value = "获取角色信息列表", notes = "获取角色信息列表")
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResponseEntity<ReturnBean> list(
            @ApiParam(value = "分页参数，从1开始") @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @ApiParam(value = "分页用") @RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
            BaseQueryBean baseQueryBean) {

        PageInfo<SysAuthUserGroup> list = new PageInfo<>();
        try {
            list = sysAuthUserGroupService.selectPageList(baseQueryBean, page, limit);
        } catch (Exception e) {
            log.error("获取当前的角色信息异常!{}",e);
        }
        return new ResponseEntity<>(new ReturnBean(list), HttpStatus.OK);
    }

    @ApiOperation(value = "添加角色", notes = "添加角色")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ReturnBean add(
            @ApiParam(value = "角色名称") @RequestParam(value = "usergroupname", required = true) String usergroupname,
            @ApiParam(value = "角色描述") @RequestParam(value = "usergroupdesc", required = false) String usergroupdesc,
            SysAuthUserGroup entity
    ) {
        log.info("======添加角色======>usergroupname:" + usergroupname + "，usergroupdesc:" + usergroupdesc);
        try {
            //验证角色是否已存在
            SysAuthUserGroup paramInfo = new SysAuthUserGroup();
            paramInfo.setUsergroupname(usergroupname);
            SysAuthUserGroup oldInfo = sysAuthUserGroupService.selectByOther(paramInfo);
            if(oldInfo != null){
                return new ReturnBean(-1, "角色名称已存在");
            }

            entity.setUsergroupid(IDManager.nextId());
            sysAuthUserGroupService.save(entity);

            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==角色管理==添加角色报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "编辑角色", notes = "编辑角色")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ReturnBean edit(
            @ApiParam(value = "角色ID") @RequestParam(value = "usergroupid", required = true) String usergroupid,
            @ApiParam(value = "角色名称") @RequestParam(value = "usergroupname", required = true) String usergroupname,
            @ApiParam(value = "角色描述") @RequestParam(value = "usergroupdesc", required = false) String usergroupdesc,
            SysAuthUserGroup entity
    ) {
        log.info("======编辑角色======>usergroupid:" + usergroupid + "，usergroupname:" + usergroupname+ "，usergroupdesc:" + usergroupdesc);
        try {
            //验证角色是否已存在
            SysAuthUserGroup paramInfo = new SysAuthUserGroup();
            paramInfo.setUsergroupname(usergroupname);
            SysAuthUserGroup oldInfo = sysAuthUserGroupService.selectByOther(paramInfo);
            if(oldInfo != null && !usergroupid.equals(oldInfo.getUsergroupid())){
                return new ReturnBean(-1, "角色名称已存在");
            }

            sysAuthUserGroupService.updateNotNull(entity);

            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==角色管理==编辑角色报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "删除角色", notes = "删除角色")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ReturnBean delete(
            @ApiParam(value = "角色ID") @RequestParam(value = "usergroupid", required = true) String usergroupid
    ) {
        log.info("======删除角色======>usergroupid:" + usergroupid);
        try {
            sysAuthUserGroupService.delete(usergroupid);

            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==角色管理==删除角色报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "获取下拉角色列表", notes = "获取下拉角色列表")
    @RequestMapping(value = "/getSelectRoleList", method = RequestMethod.GET)
    public ReturnBean getSelectRoleList(
    ) {
        try {
            return new ReturnBean(sysAuthUserGroupService.selectAll());
        }catch (Exception e){
            log.error("==角色管理==获取下拉角色列表报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    /*********************权限设置************************/

    @ApiOperation(value = "权限菜单预加载", notes = "获取下拉角色列表")
    @RequestMapping(value = "/preAuthMenu", method = RequestMethod.GET)
    public ReturnBean preAuthMenu(
            @ApiParam(value = "角色ID") @RequestParam(value = "usergroupid", required = true) String usergroupid
    ) {
        try {
            return new ReturnBean(sysAuthUserGroupService.preAuthMenu(usergroupid));
        }catch (Exception e){
            log.error("==角色管理==权限菜单预加载报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }

    @ApiOperation(value = "设置菜单权限", notes = "设置菜单权限")
    @RequestMapping(value = "/setAuthMenu", method = RequestMethod.POST)
    public ReturnBean setAuthMenu(
            @ApiParam(value = "角色ID") @RequestParam(value = "usergroupid", required = true) String usergroupid,
            @ApiParam(value = "全选的菜单ID") @RequestParam(value = "selectAllMenuids", required = false) String selectAllMenuids,
            @ApiParam(value = "半选的菜单ID") @RequestParam(value = "halfMenuids", required = false) String halfMenuids,
            SysAuthUserGroup entity
    ) {
        log.info("======设置菜单权限======>usergroupid:" + usergroupid + "，selectAllMenuids:" + selectAllMenuids+ "，halfMenuids:" + halfMenuids);
        try {
            sysAuthUserGroupService.setAuthMenu(usergroupid, selectAllMenuids, halfMenuids);

            return new ReturnBean("保存成功");
        }catch (Exception e){
            log.error("==角色管理==设置菜单权限报错==",e);
            throw new ErrorCodeException(StaticConfig.SYSTEM_ERROR);
        }
    }
}
