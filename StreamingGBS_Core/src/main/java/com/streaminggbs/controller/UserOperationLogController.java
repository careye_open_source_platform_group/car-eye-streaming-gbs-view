package com.streaminggbs.controller;

import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.ErrorCodeException;
import com.streaminggbs.common.base.ReturnBean;
import com.streaminggbs.common.config.StaticConfig;
import com.streaminggbs.entity.*;
import com.streaminggbs.interfaces.SysAuthDeptService;
import com.streaminggbs.interfaces.SysAuthUserOperationLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lilin
 * @date 2020-09-13 13:36
 * @discription 机构管理
 */
@Api(tags = {"用户行为日志列表"})
@Slf4j
@RestController
@RequestMapping("/user/log")
public class UserOperationLogController {

    @Autowired
    private SysAuthUserOperationLogService sysAuthUserOperationLogService;

    @ApiOperation(value = "获取用户行为日志列表", notes = "获取用户行为日志列表")
    @RequestMapping(value = "/findPage", method = RequestMethod.GET)
    public ResponseEntity<ReturnBean> findPage(
            @ApiParam(value = "分页参数，从1开始") @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @ApiParam(value = "分页用") @RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
            @ApiParam("开始时间yyyy-MM-dd hh:mm:ss") @RequestParam("startTime") String startTime,
            @ApiParam("结束时间yyyy-MM-dd hh:mm:ss") @RequestParam("endTime") String endTime
    ) {
        PageInfo<SysAuthUserOperationLogEntity> pageRs = new PageInfo<SysAuthUserOperationLogEntity>();
        try {
            pageRs = sysAuthUserOperationLogService.findPage( page, limit,startTime,endTime);
        } catch (Exception e) {
            log.error("获取用户行为日志异常!",e);
            return new ResponseEntity(new ReturnBean(pageRs), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(new ReturnBean(pageRs), HttpStatus.OK);
    }
}
