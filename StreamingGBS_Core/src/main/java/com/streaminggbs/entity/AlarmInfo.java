package com.streaminggbs.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Slf4j
@Data
@Table(name = "device_alarm")
public class AlarmInfo {
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "channel")
    String channel;//国标编码
    @Column(name = "device")
    String device;//国标编码
    @Column(name = "alarmmethod")
    Integer alarmmethod;//报警方法 0.电话报警 1.设备报警 2.短信报警 3.GPS报警 4.视频报警 5.设备故障报警 7.其他报警
    @Column(name = "time")
    String time;//报警时间
    @Column(name = "lon")
    String lon;//报警的经度
    @Column(name = "lat")
    String lat;//报警的纬度
    @Column(name = "alarmtype")
    Integer alarmtype;//报警类型
    @Column(name = "level")
    Integer level;//报警级别:1 为一级警情,2 为二级警情,3 为三级警情,4 为四级警情
    @Column(name = "description")
    String description;//报警描述
    @Column(name = "deptid")
    String deptid;//部门id

    @Transient
    String alarmMethodName;
}
