package com.streaminggbs.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Table(name = "device_channel_info")
@Data
public class DeviceChannelInfo implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	/**
	  *
	  */
    	@Id
	@Column(name = "id")
	private String id;

	/**
	  *
	  */
	@Column(name = "deviceid")
	private String deviceid;

	/**
	  *通道编码
	  */
	@Column(name = "channel")
	private String  channel;

	/**
	  *通道名称
	  */
	@Column(name = "channelname")
	private String channelname;


	/**
	  *是否支持云台（0不支持  1支持）
	  */
	@Column(name = "ptz_enable")
	private Integer ptzEnable;

	/**
	  *是否支持对讲（0不支持  1支持）
	  */
	@Column(name = "talk_enbale")
	private Integer talkEnbale;

	/**
	  *通道在线状态（0不在线  1在线）
	  */
	@Column(name = "status")
	private Integer status;


	/**
	 *通道类型
	 */
	@Column(name = "type")
	private Integer type;
	/**
	  *备用
	  */
	@Column(name = "bk1")
	private String bk1;

	@Transient
	private String device;

	@Transient
	private Integer sum;
}
