package com.streaminggbs.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

@Table(name = "device_info")
@Data
public class DeviceInfo implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	/**
	  *
	  */
    	@Id
	@Column(name = "id")
	private String id;

	/**
	  *组织机构
	  */
	@Column(name = "deptid")
	private String deptid;

	/**
	  *设备编码
	  */
	@Column(name = "device")
	private String device;

	/**
	  *设备类型(外键设备类型表ID）
	  */
	@Column(name = "devicetypeid")
	private String devicetypeid;

	/**
	  *设备名
	  */
	@Column(name = "devicename")
	private String devicename;

	/**
	  *IP地址
	  */
	@Column(name = "ip")
	private String ip;

	/**
	  *端口
	  */
	@Column(name = "port")
	private String port;

	/**
	  *接入密码
	  */
	@Column(name = "password")
	private String password;

	/**
	  *安装时间
	  */
	@Column(name = "install_time")
	private Date installTime;

	/**
	  *安装位置
	  */
	@Column(name = "location")
	private String location;

	/**
	  *
	  */
	@Column(name = "remark")
	private String remark;

	/**
	  *备用
	  */
	@Column(name = "bk1")
	private String bk1;

	/**
	  *备用
	  */
	@Column(name = "bk2")
	private String bk2;

	/**
	  *创建时间
	  */
	@Column(name = "create_date")
	private Date createDate;

	/**
	  *创建人
	  */
	@Column(name = "create_user")
	private String createUser;

	/**
	  *最后一次修改时间
	  */
	@Column(name = "modify_date")
	private Date modifyDate;

	/**
	  *修改记录用户
	  */
	@Column(name = "modify_user")
	private String modifyUser;

    /**
     *在线状态（0离线 1在线）
     */
    @Column(name = "status")
    private Integer status;

    /**
     *报警状态(0没报警 1报警)
     */
    @Column(name = "alarmstatus")
    private Integer alarmstatus;

    /**
     *最近上线时间
     */
    @Column(name = "on_time")
    private Date onTime;

    /**
     *经度
     */
    @Column(name = "lon")
    private String lon;

    /**
     *维度
     */
    @Column(name = "lat")
    private String lat;

	/**
	 * 位置订阅
	 */
	@Column(name = "subscribe_location_flag")
	private Boolean subscribeLocationFlag;

	@Transient
	private String deptname;//机构名称
	@Transient
	private String devicetypename;//设备类型名称
	@Transient
	private boolean isDevice = true;//是否是设备

	/**
	 *通道个数
	 */
	@Transient
	private Integer channels;

	@Transient
	private Integer sum;
}
