package com.streaminggbs.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author LiLin
 * @Date 2020/10/19 14:22
 * @Description 国标导入实体类
 */
@Data
public class GbImport implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *国标编码
     */
    private String gbId;

    /**
     *类型（0机构 1设备 2通道）
     */
    private Integer type;

    /**
     *父级国标ID
     */
    private String parentGbId;

    /**
     *名称
     */
    private String name;

    /**
     *IP地址
     */
    private String ip;

    /**
     *端口
     */
    private String port;

    /**
     *接入密码
     */
    private String password;

    /**
     *设备类型
     */
    private String devicetypename;

    /**
     *设备类型(外键设备类型表ID）
     */
    private String devicetypeid;

    private String id;

    private String parentId;


}
