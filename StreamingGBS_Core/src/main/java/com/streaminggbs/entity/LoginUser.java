package com.streaminggbs.entity;

import java.io.Serializable;

/**
 * @Author LiLin
 * @Date 2020/9/18 9:34
 * @Description 登录用户，用于存储当前登录的用户信息
 */
public class LoginUser implements Serializable {

    private SysAuthUser sysUser;

    public SysAuthUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysAuthUser sysUser) {
        this.sysUser = sysUser;
    }
}
