package com.streaminggbs.entity;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author LiLin
 * @Date 2020/9/14 12:43
 * @Description 机构Tree节点
 */
public class NodeDept implements Serializable {
    private static final long serialVersionUID = 1L;

    private String parentid;

    private String deptid;

    private String deptname;

    private boolean isDevice;//是否是设备;
    /**
     * 孩子节点列表
     */
    private List<NodeDept> children = new ArrayList<>();

    // 先序遍历，拼接JSON字符串  转换成ztree类型数据格式
    public String toZtreeString() {
        return JSON.toJSONString(this);
    }

    public List<NodeDept> getChildren() {
        return children;
    }

    public void setChildren(List<NodeDept> children) {
        this.children = children;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getDeptid() {
        return deptid;
    }

    public void setDeptid(String deptid) {
        this.deptid = deptid;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public boolean getIsDevice() {
        return isDevice;
    }

    public void setIsDevice(boolean isDevice) {
        this.isDevice = isDevice;
    }
}
