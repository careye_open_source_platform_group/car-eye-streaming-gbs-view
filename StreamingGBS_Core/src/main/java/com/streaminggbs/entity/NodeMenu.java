package com.streaminggbs.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author LiLin
 * @Date 2020/9/16 11:39
 * @Description 主页菜单node
 */
public class NodeMenu implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String path;
    private String title;
    private String icon;
    private List<NodeMenu> children = new ArrayList<>();

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<NodeMenu> getChildren() {
        return children;
    }

    public void setChildren(List<NodeMenu> children) {
        this.children = children;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
