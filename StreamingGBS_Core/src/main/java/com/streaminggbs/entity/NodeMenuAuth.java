package com.streaminggbs.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author LiLin
 * @Date 2020/9/16 11:39
 * @Description
 */
public class NodeMenuAuth implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String label;
    private List<NodeMenuAuth> children = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<NodeMenuAuth> getChildren() {
        return children;
    }

    public void setChildren(List<NodeMenuAuth> children) {
        this.children = children;
    }
}
