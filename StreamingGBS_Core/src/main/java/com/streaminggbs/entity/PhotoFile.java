package com.streaminggbs.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "photo_file")
@Data
public class PhotoFile implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	/**
	  *自增id
	  */
    @Id
	@Column(name = "id")
	private Long id;

	/**
	  *国标编码
	  */
	@Column(name = "channel")
	private String channel;

	/**
	 *国标编码
	 */
	@Column(name = "device")
	private String device;
	/**
	  *录制路径
	  */
	@Column(name = "dir")
	private String dir;
	/**
	 * 0直播1录像计划2回放3回放下载
	 */
//	@Column(name = "type")
//	private Integer type;
	/**
	  *文件名
	  */
	@Column(name = "name")
	private String name;

	/**
	  *服务器ip地址
	 */
	@Column(name = "ip")
	private String ip;

	/**
	 *端口
	 */
	@Column(name = "port")
	private Integer port;

	/**
	 *用户名
	 */
//	@Column(name = "usr")
//	private String usr;
	/**
	 *密码
	 */
//	@Column(name = "passwd")
//	private String passwd;
	/**
	 *大小
	 */
//	@Column(name = "size")
//	private Integer size;
	/**
	 *下载通信协议
	 */
//	@Column(name = "protocol")
//	private Integer protocol;

	/**
	 *视频开始时间
	 */
//	@Column(name = "starttime")
//	private String starttime;

	/**
	 *视频结束时间
	 */
//	@Column(name = "endtime")
//	private String endtime;

	/**
	 *视频结束时间
	 */
//	@Column(name = "localstarttime")
//	private String localstarttime;
	/**
	 *视频结束时间
	 */
//	@Column(name = "localendtime")
//	private String localendtime;

	@Column(name = "seq")
	private Integer seq;

	@Column(name = "result")
	private Integer result;

	@Column(name = "isdelete")
	private Integer isdelete;

	@Column(name = "deptid")
	private String deptid;

	/**
	 *视频结束时间
	 */
	@Column(name = "create_time")
	private Date createTime;
}
