package com.streaminggbs.entity;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "record_plan")
@Data
public class RecordPlan implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	/**
	  *
	  */
    @Id
	@Column(name = "id")
	private String id;


	@Column(name = "channel")
	private String channel;



	@Column(name = "content")
	private String content;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@Column(name = "update_time")
	private Date updateTime;

	@Column(name = "device")
	private String device;
}
