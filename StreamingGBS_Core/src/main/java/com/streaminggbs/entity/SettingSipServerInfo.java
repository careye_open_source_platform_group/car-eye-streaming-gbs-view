package com.streaminggbs.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "setting_sip_server_info")
public class SettingSipServerInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	    
	/**
	  *
	  */
    	@Id
	@Column(name = "id")
	private String id;
	    
	/**
	  *级联服务器名
	  */
	@Column(name = "name")
	private String name;
	    
	/**
	  *国标编码
	  */
	@Column(name = "code")
	private String code;
	    
	/**
	  *国标服务器域
	  */
	@Column(name = "domain")
	private String domain;
	    
	/**
	  *国标服务器ip
	  */
	@Column(name = "ip")
	private String ip;
	    
	/**
	  *端口
	  */
	@Column(name = "port")
	private String port;
	    
	/**
	  *用户名
	  */
	@Column(name = "user")
	private String user;
	    
	/**
	  *密码
	  */
	@Column(name = "password")
	private String password;
	    
	/**
	  *注册周期(秒)
	  */
	@Column(name = "register_peroid")
	private Integer registerPeroid;
	    
	/**
	  *心跳周期(秒)
	  */
	@Column(name = "heart_peroid")
	private Integer heartPeroid;
	    
	/**
	  *目录分组大小
	  */
	@Column(name = "gourp_size")
	private Integer gourpSize;
	    
	/**
	  *传输协议(0：UDP 1：tcp)
	  */
	@Column(name = "trans_protocol")
	private Integer transProtocol;
	    
	/**
	  *字符集(0：GB2312 1：UTF-8)
	  */
	@Column(name = "code_set")
	private Integer codeSet;
	    
	/**
	  *是否启用(0：未启用 1：启用)
	  */
	@Column(name = "active")
	private Integer active;
	    
	/**
	  *是否启用RTCP(0：未启用 1：启动)
	  */
	@Column(name = "rtcp")
	private Integer rtcp;
	    
	/**
	  *
	  */
	@Column(name = "remark")
	private String remark;
	    
	/**
	  *备用
	  */
	@Column(name = "bk1")
	private String bk1;
	    
	/**
	  *备用
	  */
	@Column(name = "bk2")
	private String bk2;
	    
	/**
	  *创建时间
	  */
	@Column(name = "create_date")
	private java.util.Date createDate;
	    
	/**
	  *创建人
	  */
	@Column(name = "create_user")
	private String createUser;
	    
	/**
	  *最后一次修改时间
	  */
	@Column(name = "modify_date")
	private java.util.Date modifyDate;
	    
	/**
	  *修改记录用户
	  */
	@Column(name = "modify_user")
	private String modifyUser;


}