package com.streaminggbs.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
public class SettingSipServerInfoUpload implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *国标编码
	 */
	private String gb_id;
	/**
	  *级联服务器名
	  */
	private String name;

	/**
	 *国标编码
	 */
	private String code;
	    
	/**
	  *国标服务器域
	  */
	private String domain;
	    
	/**
	  *国标服务器ip
	  */
	private String ip;
	    
	/**
	  *端口
	  */
	private String port;
	    
	/**
	  *用户名
	  */
	private String user;
	    
	/**
	  *密码
	  */
	private String password;
	    
	/**
	  *注册周期(秒)
	  */
	private Integer register_peroid;
	    
	/**
	  *心跳周期(秒)
	  */
	private Integer heart_peroid;
	    
	/**
	  *目录分组大小
	  */
	private Integer gourp_size;
	    
	/**
	  *传输协议(0：UDP 1：tcp)
	  */
	private String trans_protocol;
	    
	/**
	  *字符集(0：GB2312 1：UTF-8)
	  */
	private String code_set;
	    
	/**
	  *是否启用(0：未启用 1：启用)
	  */

	private Integer active;
	    
	/**
	  *是否启用RTCP(0：未启用 1：启动)
	  */

	private Integer rtcp;

}