package com.streaminggbs.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "settings_device_type")
@Data
public class SettingsDeviceType implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	/**
	  *
	  */
    @Id
	@Column(name = "id")
	private String id;

	/**
	  *协议类型(0: GB28181-2011 1:GB28181-2016 2：ONVIF 3：预留 4：其他）
	  */
	@Column(name = "protocol")
	private Integer protocol;

	/**
	  *设备类型名称
	  */
	@Column(name = "typename")
	private String typename;

	/**
	  *所属厂家
	  */
	@Column(name = "company")
	private String company;

	/**
	  *视频格式(0：H264  1：H265)
	  */
	@Column(name = "video_format")
	private Integer videoFormat;

	/**
	  *视频帧率
	  */
	@Column(name = "video_fps")
	private Integer videoFps;

	/**
	  *视频分辨率
	  */
	@Column(name = "video_resolution")
	private Integer videoResolution;

	/**
	  *音频格式（0:aac 1：G711a 2：G711U 3：G726-16bit 4：G726-24bit 5：G726-32bit 6：G726-40bit 7：G722 8：g729 9：PCMA 10：其他）
	  */
	@Column(name = "audio_format")
	private Integer audioFormat;

	/**
	  *音频通道（0：单通道 1：立体声）
	  */
	@Column(name = "audio_channels")
	private Integer audioChannels;

	/**
	  *音频采样频率
	  */
	@Column(name = "audio_freq")
	private Integer audioFreq;

	/**
	  *音频帧采样位数
	  */
	@Column(name = "audio_format_bits")
	private Integer audioFormatBits;

	/**
	  *备注
	  */
	@Column(name = "remark")
	private String remark;

	/**
	  *备用
	  */
	@Column(name = "bk1")
	private String bk1;

	/**
	  *备用
	  */
	@Column(name = "bk2")
	private String bk2;

	/**
	  *备用
	  */
	@Column(name = "bk3")
	private String bk3;

	/**
	  *创建时间
	  */
	@Column(name = "create_date")
	private java.util.Date createDate;

	/**
	  *创建人
	  */
	@Column(name = "create_user")
	private String createUser;

	/**
	  *最后一次修改时间
	  */
	@Column(name = "modify_date")
	private java.util.Date modifyDate;

	/**
	  *修改记录用户
	  */
	@Column(name = "modify_user")
	private String modifyUser;

	/**
	 *心跳时间秒
	 */
	@Column(name = "heartbeat_time")
	private Integer heartbeatTime;

	/**
	 *超时次数
	 */
	@Column(name = "heartbeat_time_out_count")
	private Integer heartbeatTimeOutCount;
}
