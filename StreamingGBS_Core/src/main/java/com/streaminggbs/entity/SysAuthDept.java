package com.streaminggbs.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Table(name = "sys_auth_dept")
@Data
public class SysAuthDept implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	/**
	  *
	  */
    	@Id
	@Column(name = "deptid")
	private String deptid;

	/**
	  *机构名称
	  */
	@Column(name = "deptname")
	private String deptname;

	/**
	  *父级ID，-1表示顶级部门
	  */
	@Column(name = "parentid")
	private String parentid;

	/**
	  *联系人
	  */
	@Column(name = "contract")
	private String contract;

	/**
	  *电话
	  */
	@Column(name = "tel")
	private String tel;

	/**
	  *地址
	  */
	@Column(name = "address")
	private String address;

	/**
	  *挂载最大设备数量（0无限制）
	  */
	@Column(name = "max_car_num")
	private Integer maxCarNum;

	/**
	  *机构类别（0部门 1单位 2其他）
	  */
	@Column(name = "dep_class")
	private Integer depClass;

	/**
	  *机构用户编码
	  */
	@Column(name = "gb_id")
	private String gbId;

	/**
	  *操作人ID
	  */
	@Column(name = "op_userid")
	private String opUserid;

	/**
	  *备注
	  */
	@Column(name = "remark")
	private String remark;

	/**
	  *创建时间
	  */
	@Column(name = "create_date")
	private java.util.Date createDate;

	/**
	  *创建用户
	  */
	@Column(name = "create_user")
	private String createUser;

	/**
	  *修改时间
	  */
	@Column(name = "modify_date")
	private java.util.Date modifyDate;

	/**
	  *修改用户
	  */
	@Column(name = "modify_user")
	private String modifyUser;

	@Column(name = "deptidnew")
	private String deptidnew ;
    @Transient
    private Integer subCount;//子机构数量
	@Transient
	private Integer subDeviceCount;//设备数量
    @Transient
    private Integer deptLevel;//机构级别

}
