package com.streaminggbs.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "sys_auth_login_log")
public class SysAuthLoginLog implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	    
	/**
	  *
	  */
    	@Id
	@Column(name = "id")
	private String id;
	    
	/**
	  *
	  */
	@Column(name = "deptid")
	private String deptid;
	    
	/**
	  *
	  */
	@Column(name = "userid")
	private String userid;
	    
	/**
	  *
	  */
	@Column(name = "logindate")
	private java.util.Date logindate;
	    
	/**
	  *
	  */
	@Column(name = "loginip")
	private String loginip;
	    
	/**
	  *是否成功,1成功,2失败
	  */
	@Column(name = "loginflag")
	private Integer loginflag;
	    
	/**
	  *
	  */
	@Column(name = "remark")
	private String remark;
	    
	/**
	  *状态1登录 2退出
	  */
	@Column(name = "status")
	private Integer status;
	    
	/**
	  *工作时长（秒）
	  */
	@Column(name = "work_time")
	private Integer workTime;

	public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
	public String getDeptid() {
        return deptid;
    }
    
    public void setDeptid(String deptid) {
        this.deptid = deptid;
    }
	public String getUserid() {
        return userid;
    }
    
    public void setUserid(String userid) {
        this.userid = userid;
    }
	public java.util.Date getLogindate() {
        return logindate;
    }
    
    public void setLogindate(java.util.Date logindate) {
        this.logindate = logindate;
    }
	public String getLoginip() {
        return loginip;
    }
    
    public void setLoginip(String loginip) {
        this.loginip = loginip;
    }
	public Integer getLoginflag() {
        return loginflag;
    }
    
    public void setLoginflag(Integer loginflag) {
        this.loginflag = loginflag;
    }
	public String getRemark() {
        return remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }
	public Integer getStatus() {
        return status;
    }
    
    public void setStatus(Integer status) {
        this.status = status;
    }
	public Integer getWorkTime() {
        return workTime;
    }
    
    public void setWorkTime(Integer workTime) {
        this.workTime = workTime;
    }
}