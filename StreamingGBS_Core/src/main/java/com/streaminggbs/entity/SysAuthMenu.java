package com.streaminggbs.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Table(name = "sys_auth_menu")
public class SysAuthMenu implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	    
	/**
	  *菜单编号
	  */
    	@Id
	@Column(name = "menuid")
	private String menuid;
	    
	/**
	  *父级ID	父菜单ID,-1表示顶级菜单
	  */
	@Column(name = "parentmenuid")
	private String parentmenuid;
	    
	/**
	  *菜单名称
	  */
	@Column(name = "menuname")
	private String menuname;
	    
	/**
	  *地址	功能访问路径
	  */
	@Column(name = "menuaddr")
	private String menuaddr;
	    
	/**
	  *是否显示	(0不显示 1显示)
	  */
	@Column(name = "displaytype")
	private Integer displaytype;
	    
	/**
	  *排序号
	  */
	@Column(name = "menusort")
	private Integer menusort;
	    
	/**
	  *菜单级别(1一级， 2 二级）
	  */
	@Column(name = "menulevel")
	private Integer menulevel;
	    
	/**
	  *菜单类型	(0-菜单  1-按钮)
	  */
	@Column(name = "menutype")
	private Integer menutype;
	    
	/**
	  *图片地址（font-awesome）
	  */
	@Column(name = "imgurl")
	private String imgurl;
	    
	/**
	  *创建时间
	  */
	@Column(name = "create_date")
	private java.util.Date createDate;
	    
	/**
	  *创建人
	  */
	@Column(name = "create_user")
	private String createUser;
	    
	/**
	  *最后一次修改时间
	  */
	@Column(name = "modify_date")
	private java.util.Date modifyDate;
	    
	/**
	  *修改记录用户
	  */
	@Column(name = "modify_user")
	private String modifyUser;

	@Transient
    private String parentmenuname;//父级菜单名称

	public String getMenuid() {
        return menuid;
    }
    
    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }
	public String getParentmenuid() {
        return parentmenuid;
    }
    
    public void setParentmenuid(String parentmenuid) {
        this.parentmenuid = parentmenuid;
    }
	public String getMenuname() {
        return menuname;
    }
    
    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }
	public String getMenuaddr() {
        return menuaddr;
    }
    
    public void setMenuaddr(String menuaddr) {
        this.menuaddr = menuaddr;
    }
	public Integer getDisplaytype() {
        return displaytype;
    }
    
    public void setDisplaytype(Integer displaytype) {
        this.displaytype = displaytype;
    }
	public Integer getMenusort() {
        return menusort;
    }
    
    public void setMenusort(Integer menusort) {
        this.menusort = menusort;
    }
	public Integer getMenulevel() {
        return menulevel;
    }
    
    public void setMenulevel(Integer menulevel) {
        this.menulevel = menulevel;
    }
	public Integer getMenutype() {
        return menutype;
    }
    
    public void setMenutype(Integer menutype) {
        this.menutype = menutype;
    }
	public String getImgurl() {
        return imgurl;
    }
    
    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }
	public java.util.Date getCreateDate() {
        return createDate;
    }
    
    public void setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
    }
	public String getCreateUser() {
        return createUser;
    }
    
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
	public java.util.Date getModifyDate() {
        return modifyDate;
    }
    
    public void setModifyDate(java.util.Date modifyDate) {
        this.modifyDate = modifyDate;
    }
	public String getModifyUser() {
        return modifyUser;
    }
    
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }

    public String getParentmenuname() {
        return parentmenuname;
    }

    public void setParentmenuname(String parentmenuname) {
        this.parentmenuname = parentmenuname;
    }
}