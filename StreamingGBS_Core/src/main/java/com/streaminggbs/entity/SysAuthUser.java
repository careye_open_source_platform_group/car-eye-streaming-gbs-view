package com.streaminggbs.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Data
@Table(name = "sys_auth_user")
public class SysAuthUser implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	/**
	  *用户id主键
	  */
    	@Id
	@Column(name = "userid")
	private String userid;

	/**
	  *组织机构	外键（机构ID）
	  */
	@Column(name = "deptid")
	private String deptid;

	/**
	  *loginname
	  */
	@Column(name = "loginname")
	private String loginname;

	/**
	  *登录密码
	  */
	@Column(name = "password")
	private String password;

	/**
	  *姓名
	  */
	@Column(name = "username")
	private String username;

	/**
	  *1 男 0 女
	  */
	@Column(name = "usersex")
	private Integer usersex;

	/**
	  *手机号
	  */
	@Column(name = "mobile")
	private String mobile;

	/**
	  *固定电话
	  */
	@Column(name = "telephone")
	private String telephone;

	/**
	  *
	  */
	@Column(name = "email")
	private String email;

	/**
	  *用户编码（用于授权）
	  */
	@Column(name = "user_number")
	private String userNumber;

	/**
	  *有效期
	  */
	@Column(name = "validtime")
	private java.util.Date validtime;

	/**
	  *状态（停用/激活状态）1 激活 0 停用 默认为：1
	  */
	@Column(name = "status")
	private Integer status;

	/**
	  *用户组id（角色ID）
	  */
	@Column(name = "usergroupid")
	private String usergroupid;

	/**
	  *登陆时间
	  */
	@Column(name = "logintime")
	private java.util.Date logintime;

	/**
	  *注销时间
	  */
	@Column(name = "exittime")
	private java.util.Date exittime;

	/**
	  *备注
	  */
	@Column(name = "remark")
	private String remark;

	/**
	  *创建时间
	  */
	@Column(name = "create_date")
	private java.util.Date createDate;

	/**
	  *创建用户
	  */
	@Column(name = "create_user")
	private String createUser;

	/**
	  *修改时间
	  */
	@Column(name = "modify_date")
	private java.util.Date modifyDate;

	/**
	  *修改用户
	  */
	@Column(name = "modify_user")
	private String modifyUser;

	@Transient
    private String deptname;

	@Transient
    private String usergroupname;


}
