package com.streaminggbs.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Table(name = "sys_auth_user_group")
public class SysAuthUserGroup implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	    
	/**
	  *用户组ID
	  */
    	@Id
	@Column(name = "usergroupid")
	private String usergroupid;
	    
	/**
	  *用户组名称
	  */
	@Column(name = "usergroupname")
	private String usergroupname;
	    
	/**
	  *用户组描述
	  */
	@Column(name = "usergroupdesc")
	private String usergroupdesc;

	/**
	  *操作人ID
	  */
	@Column(name = "op_userid")
	private String opUserid;
	    
	/**
	  *创建时间
	  */
	@Column(name = "create_date")
	private java.util.Date createDate;
	    
	/**
	  *创建用户
	  */
	@Column(name = "create_user")
	private String createUser;
	    
	/**
	  *修改时间
	  */
	@Column(name = "modify_date")
	private java.util.Date modifyDate;
	    
	/**
	  *修改用户
	  */
	@Column(name = "modify_user")
	private String modifyUser;

	public String getUsergroupid() {
        return usergroupid;
    }
    
    public void setUsergroupid(String usergroupid) {
        this.usergroupid = usergroupid;
    }
	public String getUsergroupname() {
        return usergroupname;
    }
    
    public void setUsergroupname(String usergroupname) {
        this.usergroupname = usergroupname;
    }
	public String getUsergroupdesc() {
        return usergroupdesc;
    }
    
    public void setUsergroupdesc(String usergroupdesc) {
        this.usergroupdesc = usergroupdesc;
    }
	public String getOpUserid() {
        return opUserid;
    }
    
    public void setOpUserid(String opUserid) {
        this.opUserid = opUserid;
    }
	public java.util.Date getCreateDate() {
        return createDate;
    }
    
    public void setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
    }
	public String getCreateUser() {
        return createUser;
    }
    
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
	public java.util.Date getModifyDate() {
        return modifyDate;
    }
    
    public void setModifyDate(java.util.Date modifyDate) {
        this.modifyDate = modifyDate;
    }
	public String getModifyUser() {
        return modifyUser;
    }
    
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }
}