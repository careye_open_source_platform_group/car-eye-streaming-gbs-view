package com.streaminggbs.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "sys_auth_user_group_menu")
public class SysAuthUserGroupMenu implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	    
	/**
	  *
	  */
    	@Id
	@Column(name = "id")
	private String id;
	    
	/**
	  *用户组id
	  */
	@Column(name = "usergroupid")
	private String usergroupid;
	    
	/**
	  *菜单id
	  */
	@Column(name = "menuid")
	private String menuid;
	    
	/**
	  *是否为半选中（0选中 1半选中）
	  */
	@Column(name = "is_half")
	private Integer isHalf;
	    
	/**
	  *创建时间
	  */
	@Column(name = "create_date")
	private java.util.Date createDate;
	    
	/**
	  *创建用户
	  */
	@Column(name = "create_user")
	private String createUser;
	    
	/**
	  *修改时间
	  */
	@Column(name = "modify_date")
	private java.util.Date modifyDate;
	    
	/**
	  *修改用户
	  */
	@Column(name = "modify_user")
	private String modifyUser;

	public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
	public String getUsergroupid() {
        return usergroupid;
    }
    
    public void setUsergroupid(String usergroupid) {
        this.usergroupid = usergroupid;
    }
	public String getMenuid() {
        return menuid;
    }
    
    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }
	public Integer getIsHalf() {
        return isHalf;
    }
    
    public void setIsHalf(Integer isHalf) {
        this.isHalf = isHalf;
    }
	public java.util.Date getCreateDate() {
        return createDate;
    }
    
    public void setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
    }
	public String getCreateUser() {
        return createUser;
    }
    
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
	public java.util.Date getModifyDate() {
        return modifyDate;
    }
    
    public void setModifyDate(java.util.Date modifyDate) {
        this.modifyDate = modifyDate;
    }
	public String getModifyUser() {
        return modifyUser;
    }
    
    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }
}