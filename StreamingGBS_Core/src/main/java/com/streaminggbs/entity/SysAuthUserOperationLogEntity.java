package com.streaminggbs.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

@ApiModel(value="用户操作行为日志", description="用户操作行为日志记录")
@Table(name = "sys_auth_user_operation_log")
@Data
public class SysAuthUserOperationLogEntity implements Serializable {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "操作用户")
    @Column(name = "userid")
    private String userId;

    @ApiModelProperty(value = "操作说明")
    @Column(name = "operation_content")
    private String operationContent;

    @ApiModelProperty(value = "操作时间")
    @Column(name = "start_time")
    private Date startTime;

    @ApiModelProperty(value = "结束时间")
    @Column(name = "end_time")
    private Date endTime;

    @ApiModelProperty(value = "根路径")
    @Column(name = "base_path")
    private String basePath;

    @ApiModelProperty(value = "uri")
    @Column(name = "uri")
    private String uri;

    @ApiModelProperty(value = "URL")
    @Column(name = "url")
    private String url;

    @ApiModelProperty(value = "请求类型")
    @Column(name = "method")
    private String method;

    @ApiModelProperty(value = "IP地址")
    @Column(name = "ip")
    private String ip;

    @ApiModelProperty(value = "请求参数")
    @Column(name = "parameter")
    private String parameter;

    @ApiModelProperty(value = "请求返回的结果")
    @Column(name = "result")
    private String result;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "登录用户")
    @Transient
    private String loginName;
}
