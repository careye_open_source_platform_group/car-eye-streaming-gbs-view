package com.streaminggbs.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "sys_param_set")
public class SysParamSet implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private String id;

	/**
	 * 视频关闭延迟（秒）
	 */
	@Column(name = "close_delay")
	private Integer closeDelay;

	/**
	 * 登录超时（分钟）
	 */
	@Column(name = "session_timeout")
	private Integer sessionTimeout;

	/**
	 * 订阅位置开关 true false
	 */
	@Column(name = "position_switch_flag")
	private Boolean positionSwitchFlag;

	/**
	 * 视频服务器地址
	 */
	@Column(name = "video_server_url")
	private String videoServerUrl;


}
