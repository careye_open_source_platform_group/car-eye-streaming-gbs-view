/**
 * 
 */
package com.streaminggbs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.ToString;

/**
 * 视频服务器信息
 * 
 * @author plainheart
 *
 */
@Table(name = "setting_video_server_info")
@Data
@ToString
public class VideoServerInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private String id;

	/**
	 * 视频服务器公网IP
	 */
	@Column(name = "server_ip")
	private String publicIp;

	/**
	 * 视频服务器内网IP
	 */
	@Column(name = "server_local_ip")
	private String localIp;

	/**
	 * 视频服务器名称
	 */
	@Column(name = "server_name")
	private String name;

	/**
	 * HTTP播放端口
	 */
	@Column(name = "http_port")
	private Integer httpPort;

	/**
	 * RTMP播放端口
	 */
	@Column(name = "rtmp_port")
	private Integer rtmpPort;

	/**
	 * 状态检测IP
	 */
	@Column(name = "state_check_ip")
	private String stateCheckIp;

	/**
	 * 状态检测端口
	 */
	@Column(name = "state_check_port")
	private Integer stateCheckPort;
	
	/**
	 * 是否开启录像
	 */
	@Column(name = "record")
	private Boolean record;

	/**
	 * 录制文件存放目录
	 */
	@Column(name = "record_file_dir")
	private String recordFileDir;

	/**
	 * 服务器的最大带宽（单位：Mbps）
	 */
	@Column(name = "max_bandwidth")
	private Integer maxBandwidth;

	/**
	 * 文件服务器IP
	 */
	@Column(name = "file_server_ip")
	private String fileServerIp;

	/**
	 * 文件服务器端口
	 */
	@Column(name = "file_server_port")
	private Integer fileServerPort;

	/**
	 * 该服务器是否启用
	 */
	@Column(name = "enabled")
	private Boolean enabled;
	
	/**
	 * 语音对讲IP
	 */
	@Column(name = "voice_talk_ip")
	private String voiceTalkIp;
	    
	/**
	 * 语音对讲端口
	 */
	@Column(name = "voice_talk_port")
	private String voiceTalkPort;
	
	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	@JsonFormat(timezone = "Asia/Shanghai", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	
	/**
	 * 修改时间
	 */
	@Column(name = "update_time")
	@JsonFormat(timezone = "Asia/Shanghai", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

}
