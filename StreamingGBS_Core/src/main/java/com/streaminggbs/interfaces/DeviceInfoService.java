package com.streaminggbs.interfaces;

import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.DeviceInfo;

import java.util.List;

public interface DeviceInfoService {

	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	int save(DeviceInfo entity);

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	int delete(Object key);

	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	int updateAll(DeviceInfo entity);

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	int updateNotNull(DeviceInfo entity);

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	DeviceInfo selectByKey(Object key);

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	List<DeviceInfo> selectByExample(Object example);

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	List<DeviceInfo> selectAll();

	DeviceInfo selectByOther(DeviceInfo entity);

	/**
	 * 根据条件分页获取列表
	 * @param baseQueryBean
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	PageInfo<DeviceInfo> selectPageList(BaseQueryBean baseQueryBean, int pageIndex, int pageSize);
	PageInfo<DeviceInfo> selectAllList(BaseQueryBean baseQueryBean);
	/**
	 * 添加设备
	 * @param entity
	 * @return
	 */
	int addDeviceInfo(DeviceInfo entity) throws Exception;

    /**
     * 编辑设备
     * @param entity
     * @return
     */
	int editDeviceInfo(DeviceInfo entity);

    /**
     * 删除设备
     * @param deviceid
     * @return
     */
	int deleteDeviceInfo(String deviceid,String device) throws Exception;

	boolean validGbId(String gbId, String id);

	DeviceInfo getCountByCondition(DeviceInfo deviceInfo);
	List<DeviceInfo> select(DeviceInfo entity);

	/**
	 * 修改设备状态信息
	 * @param deviceInfo
	 */
    void updateStatusByGbId(DeviceInfo deviceInfo);

	/**
	 * 修改设备的最后位置信息
	 * @param deviceInfo
	 */
	void updateCoordinateByGbId(DeviceInfo deviceInfo);

	/**
	 * 根据gbId获取设置信息
	 * @param gbId
	 * @return
	 */
	DeviceInfo getByGbId(String gbId);

	/**
	 * 添加设备和通道号信息
	 * @param deviceInfo
	 * @param deviceChannelNo
	 * @return
	 */
	int addDeviceInfoAndChannel(DeviceInfo deviceInfo, String deviceChannelNo) throws Exception;
}
