package com.streaminggbs.interfaces;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.streaminggbs.common.base.VideoReturnBean;
import com.streaminggbs.entity.DeviceInfo;
import com.streaminggbs.model.DownloadAllRecordModel;
import com.streaminggbs.model.DownloadProgressModel;

/**
 * GB28181的接口对接信息
 */
public interface Gb28181Service {

    /**
     * 实时音视操作接口
     * @param device
     * @param channel
     * @param vedioType
     * @param record
     * @param isPlan
     * @param jsonObject
     * @return
     */
    String play(String device, String channel, int vedioType, Integer record, boolean isPlan, JSONObject jsonObject);

    /**
     * 实时音视频控制从web发起
     * @param device
     * @param channel
     * @param command
     * @param closeType
     * @param switchType
     * @param isPlan
     */
    void playControl(String device, String channel, int command, int closeType, int switchType, boolean isPlan);

    /**
     * 视频录制完成应答
     * @param jSONObject
     * @return
     */
    //boolean recordFinished(JSONObject jSONObject);

    /**
     * 搜索视频列表
     * @param device
     * @param channel
     * @param memoryType
     * @param startTime
     * @param endTime
     * @return
     */
    JSONArray queryVedioList(String device, String channel, Integer memoryType, String startTime, String endTime);

    /**
     * 回放逻辑
     * @param device
     * @param channel
     * @param protocol
     * @param startTime
     * @param endTime
     * @param record
     * @param localRecordDuration
     * @return
     */
    JSONObject playBack(String device, String channel, String startTime, String endTime);

    /**
     * 回放控制
     * @param device
     * @param channel
     * @param cmd
     * @param param
     * @param streamId
     */

    void playBackControl(String streamid,String command,Integer range,Double scale);


    /**
     * 设备信息上传
     * @param jSONObject
     * @return
     */
    //boolean DeviceCata(JSONObject jSONObject);

    /**
     * 报警信息上报逻辑
     * @param jSONObject
     * @return
     */
    boolean uploadAlarm(JSONObject jSONObject);

    /**
     * 云台参数控制
     * @param device
     * @param channel
     * @param zoom
     * @param horizontal
     * @param vertical
     * @param zoom_level
     * @param horizontal_level
     * @param vertica_level
     * @param focus
     * @return
     */
    boolean yuntab(String device, String channel, int zoom, int horizontal, int vertical, int zoom_level, int horizontal_level, int vertica_level,int focus);



    /**
     * 设备上线逻辑
     * @param jsonObject
     * @return
     */
    boolean httpOnLine(JSONObject jsonObject);

    /**
     * 设备离线逻辑
     * @param jsonObject
     * @return
     */
    boolean httpoffLine(JSONObject jsonObject);

    /**
     * 远程添加设备
     * @param entity
     * @return
     */
    int addDevice(DeviceInfo entity);

    /**
     * 远程删除设备
     * @param device
     * @return
     */
    int deleteDevice(String device);

    /**
     * 设备订阅位置信息
     * @param device
     * @return
     */
    boolean subscribeAllLocation(String device);

    /**
     * 设备开启对讲
     * @param device
     * @return
     */
    JSONObject startSpeak(String device);

    /**
     * 设备 - 下载录像TO服务器
     * @param device
     * @param channel
     * @param startTime
     * @param endTime
     * @return
     */
    JSONObject downloadRecord(String device, String channel, String startTime, String endTime,Integer speed);

    /**
     * gb28181下载视频信息
     * @param device
     * @param channel
     * @param startTime
     * @param endTime
     * @param speed
     * @param seq
     * @return
     */
    JSONObject gb28181DownloadVideo(String device,String channel, String startTime, String endTime, Integer speed);

    /**
     * 获取下载进度信息
     * @param streamId
     * @return
     */
    DownloadProgressModel queryDownloadProgress(String streamId);

    /**
     * 查询视频下载记录
     * @param streamId
     * @return
     */
    DownloadAllRecordModel queryDownloadRecord(String streamId);

    /**
     * 设备广播
     * @param device
     * @param channel
     * @param source
     * @param mode
     * @return
     */
    JSONObject broadcast(String device,String channel, String source, Integer mode);

    /**
     * 取消位置订阅
     * @param device
     * @return
     */
    boolean cancelSubscribeAllLocation(String device);

    /**
     * 开始实时录像
     * @param streamid
     * @return
     */
    String startRealVideo (String device,String channel);

    /**
     * 关闭实时录像
     * @param sinkid
     * @return
     */
    String stopRealVideo(String device,String channel);

    /**
     * 获取设备在线状态
     * @param device
     * @param channel
     * @return
     */
    int online(String device, String channel);

    /**
     * 文件停止下载
     * @param streamid
     * @return
     */
    //JSONObject stopDownload(String streamid,String gbId);

    /**
     * 录像停止逻辑
     * @param device
     * @param channel
     */
    void stopSchedule(String device, String channel);

    /**
     * 开始录像计划
     * @param device
     * @param channel
     */
    void startSchedule(String device, String channel,JSONArray times);

    /**
     *松开
     * @param device
     * @param channel
     */
    String startPlanVideo(String device, String channel);

    /**
     * 获取设备通道信息
     * @param device
     * @return
     */
    JSONObject getDeviceChannel(String device);

    VideoReturnBean deviceReboot(String device);

    VideoReturnBean alarmReset(String device,String channel);
    VideoReturnBean alarmResetguard(String device,String channel);
    VideoReturnBean alarmSetguard(String device,String channel);
}
