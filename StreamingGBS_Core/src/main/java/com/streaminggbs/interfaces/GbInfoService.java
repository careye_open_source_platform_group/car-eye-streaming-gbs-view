package com.streaminggbs.interfaces;


import com.streaminggbs.entity.GbImport;
import com.streaminggbs.entity.SysAuthUser;

import java.util.List;

public interface GbInfoService {
	

    /**
     * 验证过表达导入数据
     * @param list
     * @return
     */
    List<GbImport> validGbImport(List<GbImport> list);

    /**
     * 批量保存国标导入信息
     * @param list
     * @return
     */
    int saveGbImport(List<GbImport> list, SysAuthUser opUser) throws Exception;
}
