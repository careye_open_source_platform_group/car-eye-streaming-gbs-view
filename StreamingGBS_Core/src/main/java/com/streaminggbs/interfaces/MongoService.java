package com.streaminggbs.interfaces;

import com.streaminggbs.common.mongodb.DeviceCoordinateModel;

/**
 * @version 1.0
 * @类名称：MongoDB
 * @类描述：
 * @修改备注：
 */
public interface MongoService {


    /**
     * 保存设备信息
     * @param deviceCoordinateModel
     */
    void saveDeviceCoordinate(DeviceCoordinateModel deviceCoordinateModel);

}
