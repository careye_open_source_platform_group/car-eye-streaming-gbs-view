package com.streaminggbs.interfaces;

import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.RecordPlan;

import java.util.List;

public interface RecordPlanService {
	
	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	int save(RecordPlan entity);

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	int deleteByPrimaryKey(Object key);

	int delete(RecordPlan recordPlan);
	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	int updateAll(RecordPlan entity);

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	int updateNotNull(RecordPlan entity);

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	RecordPlan selectByKey(Object key);

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	List<RecordPlan> selectByExample(Object example);

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	List<RecordPlan> selectAll();

	RecordPlan selectByOther(RecordPlan entity);
	
	/**
	 * 根据条件分页获取列表
	 * @param baseQueryBean
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	PageInfo<RecordPlan> selectPageList(BaseQueryBean baseQueryBean, int pageIndex, int pageSize);
	
}
