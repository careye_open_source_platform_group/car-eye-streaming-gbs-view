package com.streaminggbs.interfaces;

import com.github.pagehelper.PageInfo;
import com.streaminggbs.entity.SysAuthUserOperationLogEntity;

/**
 * 用户操作日志
 */
public interface SysAuthUserOperationLogService {

	/**
	 * 保存用户操作日志
	 * @param sysAuthUserOperationLogEntity
	 */
	void save(SysAuthUserOperationLogEntity sysAuthUserOperationLogEntity);

	/**
	 * 用户行为日志查询
	 * @param page
	 * @param limit
	 * @param startTime
	 * @param endTime
	 * @return
	 */
    PageInfo<SysAuthUserOperationLogEntity> findPage(int page, int limit,String startTime,String endTime);
}
