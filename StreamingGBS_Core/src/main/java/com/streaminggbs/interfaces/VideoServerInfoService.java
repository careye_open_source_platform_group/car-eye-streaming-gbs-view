/**
 * 
 */
package com.streaminggbs.interfaces;

import java.util.List;

import com.streaminggbs.entity.VideoServerInfo;

/**
 * @author plainheart
 *
 */
public interface VideoServerInfoService {

	int save(VideoServerInfo entity);

	int delete(String id);

	int updateAll(VideoServerInfo entity);

	int update(VideoServerInfo entity);

	boolean enable(String id, boolean disable);

	VideoServerInfo selectById(String id);

	List<VideoServerInfo> selectAll();

	List<VideoServerInfo> selectByEnabled(boolean enabled);

	VideoServerInfo pick();

}
