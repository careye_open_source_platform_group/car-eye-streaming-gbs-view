package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.AlarmInfo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface DeviceAlarmMapperR extends Mapper<AlarmInfo> {

    List<AlarmInfo> selectPageList(BaseQueryBean baseQueryBean);
}