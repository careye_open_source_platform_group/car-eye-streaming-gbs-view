package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.DeviceChannelInfo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface DeviceChannelInfoMapperR extends Mapper<DeviceChannelInfo> {

    List<DeviceChannelInfo> selectPageList(BaseQueryBean baseQueryBean);

    DeviceChannelInfo getCountByCondition(DeviceChannelInfo deviceChannelInfo);

}