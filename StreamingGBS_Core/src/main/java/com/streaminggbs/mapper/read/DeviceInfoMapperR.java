package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.DeviceInfo;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface DeviceInfoMapperR extends Mapper<DeviceInfo> {

    List<DeviceInfo> selectPageList(BaseQueryBean baseQueryBean);

    DeviceInfo getCountByCondition(DeviceInfo deviceInfo);

    /**
     * 获取位置订阅的信息
     * @param device
     * @return
     */
    DeviceInfo getByGbId(@Param("device") String device);

    /**
     * 验证设备是否存在
     * @param device
     * @return
     */
    Integer existByGbId(String device);
}
