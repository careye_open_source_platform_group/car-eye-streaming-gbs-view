package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.PhotoFile;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface PhotoFileMapperR extends Mapper<PhotoFile> {

    List<PhotoFile> selectPageList(BaseQueryBean baseQueryBean);
}