package com.streaminggbs.mapper.read;

import com.streaminggbs.entity.RecordFile;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface RecordFileMapperR extends Mapper<RecordFile> {

    List<RecordFile> selectPageList(RecordFile recordFile);
}