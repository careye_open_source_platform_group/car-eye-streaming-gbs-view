package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.RecordPlan;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface RecordPlanMapperR extends Mapper<RecordPlan> {

    List<RecordPlan> selectPageList(BaseQueryBean baseQueryBean);
}