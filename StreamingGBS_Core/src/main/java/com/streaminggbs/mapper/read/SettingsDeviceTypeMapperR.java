package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SettingsDeviceType;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SettingsDeviceTypeMapperR extends Mapper<SettingsDeviceType> {

    List<SettingsDeviceType> selectPageList(BaseQueryBean baseQueryBean);
}