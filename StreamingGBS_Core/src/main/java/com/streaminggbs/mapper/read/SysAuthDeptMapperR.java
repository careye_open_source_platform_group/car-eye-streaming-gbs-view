package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SysAuthDept;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SysAuthDeptMapperR extends Mapper<SysAuthDept> {

    List<SysAuthDept> selectPageList(BaseQueryBean baseQueryBean);

    List<SysAuthDept> getChildListByParentid(@Param("parentid") String parentid);

    List<SysAuthDept> getSelectTreeDeptList(@Param("deptid") String deptid);

    String getNextDeptIdNew(String parentid);
}