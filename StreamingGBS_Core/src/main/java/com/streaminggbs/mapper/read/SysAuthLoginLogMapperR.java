package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SysAuthLoginLog;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SysAuthLoginLogMapperR extends Mapper<SysAuthLoginLog> {

    List<SysAuthLoginLog> selectPageList(BaseQueryBean baseQueryBean);

    SysAuthLoginLog getLastLoginInfo(@Param("userid") String userid);
}