package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SysAuthMenu;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SysAuthMenuMapperR extends Mapper<SysAuthMenu> {

    List<SysAuthMenu> selectPageList(BaseQueryBean baseQueryBean);

    List<SysAuthMenu> getAuthMenuList(@Param("usergroupid") String usergroupid);

    List<String> getAuthButtonList(@Param("usergroupid") String usergroupid);
}