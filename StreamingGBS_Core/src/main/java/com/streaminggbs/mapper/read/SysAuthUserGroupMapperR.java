package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SysAuthUserGroup;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SysAuthUserGroupMapperR extends Mapper<SysAuthUserGroup> {

    List<SysAuthUserGroup> selectPageList(BaseQueryBean baseQueryBean);
}