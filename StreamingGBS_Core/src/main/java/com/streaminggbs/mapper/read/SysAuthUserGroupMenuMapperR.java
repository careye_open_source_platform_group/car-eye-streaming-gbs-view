package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SysAuthUserGroupMenu;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SysAuthUserGroupMenuMapperR extends Mapper<SysAuthUserGroupMenu> {

    List<SysAuthUserGroupMenu> selectPageList(BaseQueryBean baseQueryBean);
}