package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SysAuthUser;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SysAuthUserMapperR extends Mapper<SysAuthUser> {

    List<SysAuthUser> selectPageList(BaseQueryBean baseQueryBean);
}