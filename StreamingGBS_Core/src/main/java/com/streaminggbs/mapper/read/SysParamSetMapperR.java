package com.streaminggbs.mapper.read;

import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SysParamSet;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SysParamSetMapperR extends Mapper<SysParamSet> {

    List<SysParamSet> selectPageList(BaseQueryBean baseQueryBean);
}