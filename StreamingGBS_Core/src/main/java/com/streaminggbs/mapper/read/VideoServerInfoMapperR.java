package com.streaminggbs.mapper.read;

import com.streaminggbs.entity.VideoServerInfo;

import tk.mybatis.mapper.common.Mapper;

public interface VideoServerInfoMapperR extends Mapper<VideoServerInfo> {

}