package com.streaminggbs.mapper.write;

import com.streaminggbs.entity.AlarmInfo;
import tk.mybatis.mapper.common.Mapper;

public interface DeviceAlarmMapperW extends Mapper<AlarmInfo> {

    int deleteAll();
}