package com.streaminggbs.mapper.write;

import com.streaminggbs.entity.DeviceChannelInfo;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

public interface DeviceChannelInfoMapperW extends Mapper<DeviceChannelInfo> {

    int deleteByDeviceId(@Param("deviceid") String deviceid);

    /**
     * 修改设备的状态
     * @param deviceChannelInfo
     */
    void updateStatusByGbId(@Param("deviceChannelInfo")DeviceChannelInfo deviceChannelInfo);

    int deleteChannelChildAndOwnByDeptIdNew(@Param("deptIdNew") String deptIdNew);
}