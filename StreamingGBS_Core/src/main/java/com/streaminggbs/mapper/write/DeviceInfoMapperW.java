package com.streaminggbs.mapper.write;

import com.streaminggbs.entity.DeviceInfo;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

public interface DeviceInfoMapperW extends Mapper<DeviceInfo> {

    /**
     * 修改设备状态信息
     * @param deviceInfo
     */
    void updateStatusByGbId(@Param("deviceInfo") DeviceInfo deviceInfo);

    /**
     * 修改坐标信息
     * @param deviceInfo
     */
    void updateCoordinateByGbId(@Param("deviceInfo")DeviceInfo deviceInfo);

    int deleteDeviceChildAndOwnByDeptIdNew(@Param("deptIdNew") String deptIdNew);

}