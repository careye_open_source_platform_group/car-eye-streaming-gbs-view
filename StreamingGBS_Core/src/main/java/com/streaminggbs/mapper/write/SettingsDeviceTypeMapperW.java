package com.streaminggbs.mapper.write;

import com.streaminggbs.entity.SettingsDeviceType;
import tk.mybatis.mapper.common.Mapper;

public interface SettingsDeviceTypeMapperW extends Mapper<SettingsDeviceType> {


}