package com.streaminggbs.mapper.write;

import com.streaminggbs.entity.SysAuthDept;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

public interface SysAuthDeptMapperW extends Mapper<SysAuthDept> {

    int deleteDeptChildAndOwnByDeptIdNew(@Param("deptIdNew") String deptIdNew);
}