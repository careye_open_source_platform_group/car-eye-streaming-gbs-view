package com.streaminggbs.mapper.write;

import com.streaminggbs.entity.SysAuthUserOperationLogEntity;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 用户行为日志
 */
public interface SysAuthUserOperationLogW extends Mapper<SysAuthUserOperationLogEntity> {

    /**
     * 查询分页日志
     * @param startTime
     * @param endTime
     * @return
     */
    List<SysAuthUserOperationLogEntity> findPage(@Param("startTime") String startTime,@Param("endTime") String endTime);
}