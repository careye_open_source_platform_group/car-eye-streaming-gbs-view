package com.streaminggbs.mapper.write;

import com.streaminggbs.entity.SysParamSet;
import tk.mybatis.mapper.common.Mapper;

public interface SysParamSetMapperW extends Mapper<SysParamSet> {

    int deleteAll();
}