package com.streaminggbs.mapper.write;

import com.streaminggbs.entity.VideoServerInfo;

import tk.mybatis.mapper.common.Mapper;

public interface VideoServerInfoMapperW extends Mapper<VideoServerInfo> {

}