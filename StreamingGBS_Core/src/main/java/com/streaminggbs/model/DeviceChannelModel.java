package com.streaminggbs.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(value="设备通道信息", description="设备通道信息")
@Data
public class DeviceChannelModel {

    private String ID;
    private String Name;
    private String Manufacturer;
    private String Model;
    private String Owner;
    private String CivilCode;
    private String Address;
    private String ParentID;
    private Integer RegisterWay;
    private String Status;
}
