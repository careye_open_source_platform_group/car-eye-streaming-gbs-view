package com.streaminggbs.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 查询下载记录信息
 */
@ApiModel(value="查询所有下载记录信息", description="查询所有下载记录信息")
@Data
public class DownloadAllRecordModel implements Serializable {

    @ApiModelProperty(value = "点播服务器地址")
    private String vod;
    @ApiModelProperty(value = "查询的视频下载记录信息")
    private List<DownloadQueryRecordModel> downloadQueryRecordModels;
}
