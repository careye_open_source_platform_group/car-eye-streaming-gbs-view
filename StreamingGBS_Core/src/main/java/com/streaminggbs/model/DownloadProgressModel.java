package com.streaminggbs.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 查询下载进度信息
 */
@ApiModel(value="查询下载进度信息", description="查询下载进度信息")
@Data
public class DownloadProgressModel implements Serializable {

    @ApiModelProperty(value = "已经下载时长,单位毫秒")
    private Integer duration;
    @ApiModelProperty(value = "下载的开始时间,距离1970的秒值")
    private Integer start;
    @ApiModelProperty(value = "下载的结束时间,距离1970的秒值")
    private Integer stop;
    @ApiModelProperty(value = "-1,下载任务不存在; 0,正在下载; 1下载完成;")
    private Integer status;
}
