package com.streaminggbs.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 查询下载记录信息
 */
@ApiModel(value="搜索下载记录信息", description="搜索下载记录信息")
@Data
public class DownloadQueryRecordModel implements Serializable {

    @ApiModelProperty(value = "sinkId,用于查询进度")
    private String stream;
    @ApiModelProperty(value = "通道ID")
    private String serial;
    @ApiModelProperty(value = "文件时长，单位毫秒")
    private String duration;
    @ApiModelProperty(value = "文件大小，单位byte")
    private Integer size;
    @ApiModelProperty(value = "文件存储的相对路径")
    private String path;
    @ApiModelProperty(value = "开始时间，距离1970的秒值")
    private String start;
    @ApiModelProperty(value = "结束时间，距离1970的秒值")
    private String stop;
    @ApiModelProperty(value = "开始时间")
    private String starttime;
    @ApiModelProperty(value = "结束时间")
    private String endtime;
    @ApiModelProperty(value = "文件名")
    private String fileName;
    @ApiModelProperty(value = "录像标签")
    private String tag;
    @ApiModelProperty(value = "点播服务器地址")
    private String vod;
}
