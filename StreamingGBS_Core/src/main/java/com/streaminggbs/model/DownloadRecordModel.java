package com.streaminggbs.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 查询下载记录信息
 */
@ApiModel(value="查询下载记录信息", description="查询下载记录信息")
@Data
public class DownloadRecordModel implements Serializable {

    @ApiModelProperty(value = "sinkId,用于查询进度")
    private String sinkId;
    @ApiModelProperty(value = "streamId,用于停止下载")
    private String streamId;
    @ApiModelProperty(value = "sinkId,用于查询进度")
    private String url;

}
