package com.streaminggbs.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@ApiModel(value="录像计划配置", description="录像计划配置")
@Data
public class PlanData implements Serializable {
    @ApiModelProperty(value = "设备")
    private Integer day;

    @ApiModelProperty(value = "")
    private List<PlanTimeData> times;
}
