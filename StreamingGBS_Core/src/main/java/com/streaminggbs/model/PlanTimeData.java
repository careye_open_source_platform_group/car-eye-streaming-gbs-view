package com.streaminggbs.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@ApiModel(value="录像计划配置", description="录像计划配置")
@Data
public class PlanTimeData implements Serializable {
    @ApiModelProperty(value = "开始时间")
    private Integer stime;

    @ApiModelProperty(value = "结束时间")
    private Integer etime;
}
