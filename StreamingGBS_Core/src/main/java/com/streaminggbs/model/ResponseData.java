package com.streaminggbs.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class ResponseData<T> {
    String GB_ID;
    Integer errCode;
    String resultMsg;
    List<T> resultData = new ArrayList<T>();
}
