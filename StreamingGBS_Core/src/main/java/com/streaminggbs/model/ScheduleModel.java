package com.streaminggbs.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@ApiModel(value="录像计划发送到视频服务器数据", description="录像计划发送到视频服务器数据")
@Data
public class ScheduleModel implements Serializable {
    @ApiModelProperty(value = "设备")
    private String device;
    @ApiModelProperty(value = "通道")
    private String channel;
    @ApiModelProperty(value = "")
    private List<ScheduleTimeModel> times;
}
