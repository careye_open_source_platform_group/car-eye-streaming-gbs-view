package com.streaminggbs.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@ApiModel(value="", description="")
@Data
public class ScheduleTimeModel implements Serializable {
    @ApiModelProperty(value = "")
    private Integer schedule;
    @ApiModelProperty(value = "0-6")
   private Integer day;
    @ApiModelProperty(value = "时间范围")
    private List<ScheduleTimeRangeHourModel> ranges;
}
