package com.streaminggbs.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@ApiModel(value="", description="")
@Data
public class ScheduleTimeRangeHourModel implements Serializable {
    @ApiModelProperty(value = "小时")
    private Integer hour;
    @ApiModelProperty(value = "分钟")
    private Integer min;
    @ApiModelProperty(value = "分钟")
    private Integer second;
}
