package com.streaminggbs.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value="服务返回结果值", description="服务返回结果值")
@Data
public class ServerResultData implements Serializable {
    @ApiModelProperty(value = "国标编码")
    String GB_ID;
    @ApiModelProperty(value = "级联服务器名")
    String name;
    @ApiModelProperty(value = "code")
    String code;
    @ApiModelProperty(value = "国标服务器域")
    String domain;
    @ApiModelProperty(value = "国标服务器ip")
    String ip;
    @ApiModelProperty(value = "国标服务器端口")
    String port;
    @ApiModelProperty(value = "用户名")
    String user;
    @ApiModelProperty(value = "密码")
    String password;
    @ApiModelProperty(value = "注册周期")
    String register_peroid;
    @ApiModelProperty(value = "心跳周期")
    String heart_peroid;
    @ApiModelProperty(value = "目录分组大小")
    String group_size;
    @ApiModelProperty(value = "传输协议")
    String trans_protocol;
    @ApiModelProperty(value = "字符集")
    String code_set;
    @ApiModelProperty(value = "是否启用")
    String active;
    @ApiModelProperty(value = "是否启动rtcp")
    String rtcp;
}
