package com.streaminggbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.DeviceChannelInfo;
import com.streaminggbs.interfaces.DeviceChannelInfoService;
import com.streaminggbs.interfaces.GbInfoService;
import com.streaminggbs.mapper.read.DeviceChannelInfoMapperR;
import com.streaminggbs.mapper.write.DeviceChannelInfoMapperW;
import com.streaminggbs.common.utils.IDManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class DeviceChannelInfoServiceImpl implements DeviceChannelInfoService {

	@Autowired(required = false)
	private DeviceChannelInfoMapperR deviceChannelInfoMapperR;
	@Autowired(required = false)
	private DeviceChannelInfoMapperW deviceChannelInfoMapperW;


	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	@Override
	public int save(DeviceChannelInfo entity) {
		// TODO Auto-generated method stub
		return deviceChannelInfoMapperW.insertSelective(entity);
	}

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	@Override
	public int delete(Object key) {
		// TODO Auto-generated method stub
		return deviceChannelInfoMapperW.deleteByPrimaryKey(key);
	}

	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	@Override
	public int updateAll(DeviceChannelInfo entity) {
		// TODO Auto-generated method stub
		return deviceChannelInfoMapperW.updateByPrimaryKey(entity);
	}

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	@Override
	public int updateNotNull(DeviceChannelInfo entity) {
		// TODO Auto-generated method stub
		return deviceChannelInfoMapperW.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	@Override
	public DeviceChannelInfo selectByKey(Object key) {
		// TODO Auto-generated method stub
		return deviceChannelInfoMapperR.selectByPrimaryKey(key);
	}

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	@Override
	public List<DeviceChannelInfo> selectByExample(Object example) {
		// TODO Auto-generated method stub
		return deviceChannelInfoMapperR.selectByExample(example);
	}

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public List<DeviceChannelInfo> selectAll() {
		// TODO Auto-generated method stub
		return deviceChannelInfoMapperR.selectAll();
	}
	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public DeviceChannelInfo getCountByCondition(DeviceChannelInfo deviceChannelInfo) {
		// TODO Auto-generated method stub
		return deviceChannelInfoMapperR.getCountByCondition(deviceChannelInfo);
	}
	@Override
	public DeviceChannelInfo selectByOther(DeviceChannelInfo entity) {
		// TODO Auto-generated method stub
		return deviceChannelInfoMapperR.selectOne(entity);
	}
	@Override
	public List<DeviceChannelInfo> select(DeviceChannelInfo entity) {
		// TODO Auto-generated method stub
		return deviceChannelInfoMapperR.select(entity);
	}

	@Override
	public void updateStatusByGbId(DeviceChannelInfo deviceChannelInfo) {
		deviceChannelInfoMapperW.updateStatusByGbId(deviceChannelInfo);
	}

	/**
	 * 根据条件分页获取列表
	 * @param baseQueryBean
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Override
	public PageInfo<DeviceChannelInfo> selectPageList(BaseQueryBean baseQueryBean,int pageIndex,int pageSize){
		PageHelper.startPage(pageIndex, pageSize, true);
		return new PageInfo<>(deviceChannelInfoMapperR.selectPageList(baseQueryBean));
	}
	@Override
	public PageInfo<DeviceChannelInfo> selectList(BaseQueryBean baseQueryBean){
		return new PageInfo<>(deviceChannelInfoMapperR.selectPageList(baseQueryBean));
	}
	@Override
	public boolean validGbId(String gbId,String deviceId, String id) {
		DeviceChannelInfo paramGbInfo = new DeviceChannelInfo();
		paramGbInfo.setChannel(gbId);
		paramGbInfo.setDeviceid(deviceId);
		DeviceChannelInfo oldEntity = selectByOther(paramGbInfo);
		if(oldEntity != null && !oldEntity.getId().equals(id)){
			return false;
		}
		return true;
	}
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int addChannelInfo(DeviceChannelInfo entity) {
        //验证设备是否已存在
        boolean isValid = validGbId(entity.getChannel(), entity.getDeviceid(),null);
        if(!isValid){
            log.info("通道编号已存在，deviceid=" + entity.getDeviceid() + ", channel=" + entity.getChannel());
            return -1;
        }
        entity.setId(IDManager.nextId());
        //插入设备信息
        save(entity);

        return 1;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int editChannelInfo(DeviceChannelInfo entity) {
        //验证设备是否已存在
        boolean isValid = validGbId(entity.getChannel(), entity.getDeviceid(),entity.getId());
        if(!isValid){
            log.info("通道编号已存在，deviceid=" + entity.getDeviceid() + ", channel=" + entity.getChannel());
            return -1;
        }
        //更新设备
        updateNotNull(entity);

        return 1;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteChannelInfo(String id) {
        //删除国标总表、通道表信息
        //gbInfoService.deleteAllChildAndMOwn(id);
		deviceChannelInfoMapperW.deleteByPrimaryKey(id);
        return 1;
    }

}
