package com.streaminggbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.DeviceChannelInfo;
import com.streaminggbs.entity.DeviceInfo;
import com.streaminggbs.interfaces.DeviceChannelInfoService;
import com.streaminggbs.interfaces.DeviceInfoService;
import com.streaminggbs.interfaces.Gb28181Service;
import com.streaminggbs.interfaces.GbInfoService;
import com.streaminggbs.mapper.read.DeviceInfoMapperR;
import com.streaminggbs.mapper.write.DeviceChannelInfoMapperW;
import com.streaminggbs.mapper.write.DeviceInfoMapperW;
import com.streaminggbs.common.utils.IDManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class DeviceInfoServiceImpl implements DeviceInfoService {

	@Autowired(required = false)
	private DeviceInfoMapperR deviceInfoMapperR;
	@Autowired(required = false)
	private DeviceInfoMapperW deviceInfoMapperW;
	@Autowired
	private GbInfoService gbInfoService;
	@Autowired
	Gb28181Service gb28181Service;
	@Autowired
	private DeviceChannelInfoService deviceChannelInfoService;
	@Autowired
	private DeviceChannelInfoMapperW deviceChannelInfoMapperW;
	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	@Override
	public int save(DeviceInfo entity) {
		// TODO Auto-generated method stub
		return deviceInfoMapperW.insertSelective(entity);
	}

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	@Override
	public int delete(Object key) {
		// TODO Auto-generated method stub
		return deviceInfoMapperW.deleteByPrimaryKey(key);
	}

	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	@Override
	public int updateAll(DeviceInfo entity) {
		// TODO Auto-generated method stub
		return deviceInfoMapperW.updateByPrimaryKey(entity);
	}

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	@Override
	public int updateNotNull(DeviceInfo entity) {
		// TODO Auto-generated method stub
		return deviceInfoMapperW.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	@Override
	public DeviceInfo selectByKey(Object key) {
		// TODO Auto-generated method stub
		return deviceInfoMapperR.selectByPrimaryKey(key);
	}

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	@Override
	public List<DeviceInfo> selectByExample(Object example) {
		// TODO Auto-generated method stub
		return deviceInfoMapperR.selectByExample(example);
	}

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public List<DeviceInfo> selectAll() {
		// TODO Auto-generated method stub
		return deviceInfoMapperR.selectAll();
	}
	@Override
	public DeviceInfo getCountByCondition(DeviceInfo deviceInfo) {
		// TODO Auto-generated method stub
		return deviceInfoMapperR.getCountByCondition(deviceInfo);
	}
	@Override
	public DeviceInfo selectByOther(DeviceInfo entity) {
		// TODO Auto-generated method stub
		return deviceInfoMapperR.selectOne(entity);
	}
	@Override
	public List<DeviceInfo> select(DeviceInfo entity) {
		// TODO Auto-generated method stub
		return deviceInfoMapperR.select(entity);
	}

	@Override
	public void updateStatusByGbId(DeviceInfo deviceInfo) {
		deviceInfoMapperW.updateStatusByGbId(deviceInfo);
	}

	@Override
	public void updateCoordinateByGbId(DeviceInfo deviceInfo) {
		deviceInfoMapperW.updateCoordinateByGbId(deviceInfo);
	}

	@Override
	public DeviceInfo getByGbId(String gbId) {
		return deviceInfoMapperR.getByGbId(gbId);
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public int addDeviceInfoAndChannel(DeviceInfo deviceInfo, String deviceChannelNo) throws Exception {
		addDeviceInfo(deviceInfo);
		// 添加设备的通道信息
		DeviceChannelInfo deviceChannelInfo = new DeviceChannelInfo();
		deviceChannelInfo.setId(IDManager.nextId());
		deviceChannelInfo.setDeviceid(deviceInfo.getId());
		deviceChannelInfo.setChannel(deviceChannelNo);
		deviceChannelInfo.setChannelname(deviceInfo.getDevice()+"通道号");
		deviceChannelInfo.setPtzEnable(0);
		deviceChannelInfo.setTalkEnbale(0);
		deviceChannelInfo.setStatus(0);
		deviceChannelInfo.setType(0);
		deviceChannelInfoService.addChannelInfo(deviceChannelInfo);
		return 0;
	}

	/**
	 * 根据条件分页获取列表
	 * @param baseQueryBean
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Override
	public PageInfo<DeviceInfo> selectPageList(BaseQueryBean baseQueryBean, int pageIndex, int pageSize){
		PageHelper.startPage(pageIndex, pageSize, true);
		return new PageInfo<>(deviceInfoMapperR.selectPageList(baseQueryBean));
	}
	@Override
	public PageInfo<DeviceInfo> selectAllList(BaseQueryBean baseQueryBean){
		//PageHelper.startPage(pageIndex, pageSize, true);
		return new PageInfo<>(deviceInfoMapperR.selectPageList(baseQueryBean));
	}
	@Override
	public boolean validGbId(String gbId, String id) {
		DeviceInfo paramGbInfo = new DeviceInfo();
		paramGbInfo.setDevice(gbId);
		DeviceInfo oldEntity = selectByOther(paramGbInfo);
		if(oldEntity != null && !oldEntity.getId().equals(id)){
			return false;
		}
		return true;
	}
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int addDeviceInfo(DeviceInfo entity) throws Exception {
		//验证设备是否已存在
		boolean isValid = validGbId(entity.getDevice(), null);
		if(!isValid){
			log.info("设备编号已存在，deptid=" + entity.getDeptid() + ", device=" + entity.getDevice());
			return -1;
		}
		entity.setId(IDManager.nextId());
		// 验证设备号的唯一性
		if (deviceInfoMapperR.existByGbId(entity.getDevice()) > 0) {
			return -1;
		}
		//插入设备信息
		save(entity);
		// 验证设备是否存在如果不存在则新增
		int code = gb28181Service.addDevice(entity);
		if (code != 200) {
			throw new Exception("数据添加失败!");
		}

		return 1;
	}

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int editDeviceInfo(DeviceInfo entity) {
        //验证设备是否已存在
        boolean isValid = validGbId(entity.getDevice(), entity.getId());
        if(!isValid){
            log.info("设备编号已存在，deptid=" + entity.getDeptid() + ", device=" + entity.getDevice());
            return -1;
        }
        //更新设备
        updateNotNull(entity);
        //更新国标信息总表

        return 1;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteDeviceInfo(String deviceid,String device) throws Exception {
        //删除国标总表、设备表、通道表信息
        //gbInfoService.deleteAllChildAndMOwn(deviceid);
		int code = gb28181Service.deleteDevice(device);
		deviceInfoMapperW.deleteByPrimaryKey(deviceid);
		deviceChannelInfoMapperW.deleteByDeviceId(deviceid);
		if (code != 200) {
			throw new Exception("数据删除失败!");
		}
        return 1;
    }
}
