package com.streaminggbs.service;

import com.streaminggbs.common.mongodb.DeviceCoordinateModel;
import com.streaminggbs.interfaces.MongoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


/**
 * @version 1.0
 * @类名称：MongoDB
 * @类描述：
 * @修改备注：
 */
@Slf4j
//@Service
public class MongoServiceImpl implements MongoService {

    //@Value("${spring.data.mongodb.mongo-position-collection-prefix}")
    private String mongo_position_collection_prefix;

    @Autowired
    private MongoTemplate mongoTemplate;


    /**
     * 保存设备坐标信息
     * @param deviceCoordinateModel
     */
    @Override
    public void saveDeviceCoordinate(DeviceCoordinateModel deviceCoordinateModel) {
        mongoTemplate.save(deviceCoordinateModel,mongo_position_collection_prefix);
    }

}
