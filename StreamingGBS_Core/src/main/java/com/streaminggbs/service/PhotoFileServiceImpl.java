package com.streaminggbs.service;

import com.streaminggbs.entity.PhotoFile;
import com.streaminggbs.interfaces.PhotoFileService;
import com.streaminggbs.mapper.read.PhotoFileMapperR;
import com.streaminggbs.mapper.write.PhotoFileMapperW;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class PhotoFileServiceImpl implements PhotoFileService {

	@Autowired(required = false)
	private PhotoFileMapperR photoFileMapperR;
	@Autowired(required = false)
	private PhotoFileMapperW photoFileMapperW;

	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	@Override
	public int save(PhotoFile entity) {
		// TODO Auto-generated method stub
		return photoFileMapperW.insertSelective(entity);
	}

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	@Override
	public int delete(Object key) {
		// TODO Auto-generated method stub
		return photoFileMapperW.deleteByPrimaryKey(key);
	}

	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	@Override
	public int updateAll(PhotoFile entity) {
		// TODO Auto-generated method stub
		return photoFileMapperW.updateByPrimaryKey(entity);
	}

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	@Override
	public int updateNotNull(PhotoFile entity) {
		// TODO Auto-generated method stub
		return photoFileMapperW.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	@Override
	public PhotoFile selectByKey(Object key) {
		// TODO Auto-generated method stub
		return photoFileMapperR.selectByPrimaryKey(key);
	}

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	@Override
	public List<PhotoFile> selectByExample(Object example) {
		// TODO Auto-generated method stub
		return photoFileMapperR.selectByExample(example);
	}

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public List<PhotoFile> selectAll() {
		// TODO Auto-generated method stub
		return photoFileMapperR.selectAll();
	}

	@Override
	public PhotoFile selectByOther(PhotoFile entity) {
		// TODO Auto-generated method stub
		return photoFileMapperR.selectOne(entity);
	}
	

}