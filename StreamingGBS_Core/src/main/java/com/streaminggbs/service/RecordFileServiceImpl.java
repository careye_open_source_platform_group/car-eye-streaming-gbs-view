package com.streaminggbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.entity.RecordFile;
import com.streaminggbs.interfaces.RecordFileService;
import com.streaminggbs.mapper.read.RecordFileMapperR;
import com.streaminggbs.mapper.write.RecordFileMapperW;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class RecordFileServiceImpl implements RecordFileService {

	@Autowired(required = false)
	private RecordFileMapperR recordFileMapperR;
	@Autowired(required = false)
	private RecordFileMapperW recordFileMapperW;

	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	@Override
	public int save(RecordFile entity) {
		// TODO Auto-generated method stub
		return recordFileMapperW.insertSelective(entity);
	}

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	@Override
	public int delete(Object key) {
		// TODO Auto-generated method stub
		return recordFileMapperW.deleteByPrimaryKey(key);
	}

	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	@Override
	public int updateAll(RecordFile entity) {
		// TODO Auto-generated method stub
		return recordFileMapperW.updateByPrimaryKey(entity);
	}

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	@Override
	public int updateNotNull(RecordFile entity) {
		// TODO Auto-generated method stub
		return recordFileMapperW.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	@Override
	public RecordFile selectByKey(Object key) {
		// TODO Auto-generated method stub
		return recordFileMapperR.selectByPrimaryKey(key);
	}

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	@Override
	public List<RecordFile> selectByExample(Object example) {
		// TODO Auto-generated method stub
		return recordFileMapperR.selectByExample(example);
	}

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public List<RecordFile> selectAll() {
		// TODO Auto-generated method stub
		return recordFileMapperR.selectAll();
	}

	@Override
	public RecordFile selectByOther(RecordFile entity) {
		// TODO Auto-generated method stub
		return recordFileMapperR.selectOne(entity);
	}
	@Override
	public PageInfo<RecordFile> selectPageList(RecordFile recordFile, int pageIndex, int pageSize){
		PageHelper.startPage(pageIndex, pageSize, true);
		return new PageInfo<>(recordFileMapperR.selectPageList(recordFile));
	}
	@Override
	public List<RecordFile> selectAllList(RecordFile recordFile){
		return recordFileMapperR.selectPageList(recordFile);
	}
}