package com.streaminggbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.RecordPlan;
import com.streaminggbs.interfaces.RecordPlanService;
import com.streaminggbs.mapper.read.RecordPlanMapperR;
import com.streaminggbs.mapper.write.RecordPlanMapperW;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class RecordPlanServiceImpl implements RecordPlanService {

	@Autowired(required = false)
	private RecordPlanMapperR recordPlanMapperR;
	@Autowired(required = false)
	private RecordPlanMapperW recordPlanMapperW;

	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	@Override
	public int save(RecordPlan entity) {
		// TODO Auto-generated method stub
		return recordPlanMapperW.insertSelective(entity);
	}

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	@Override
	public int deleteByPrimaryKey(Object key) {
		// TODO Auto-generated method stub
		return recordPlanMapperW.deleteByPrimaryKey(key);
	}
	@Override
	public int delete(RecordPlan key) {
		// TODO Auto-generated method stub
		return recordPlanMapperW.delete(key);
	}
	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	@Override
	public int updateAll(RecordPlan entity) {
		// TODO Auto-generated method stub
		return recordPlanMapperW.updateByPrimaryKey(entity);
	}

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	@Override
	public int updateNotNull(RecordPlan entity) {
		// TODO Auto-generated method stub
		return recordPlanMapperW.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	@Override
	public RecordPlan selectByKey(Object key) {
		// TODO Auto-generated method stub
		return recordPlanMapperR.selectByPrimaryKey(key);
	}

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	@Override
	public List<RecordPlan> selectByExample(Object example) {
		// TODO Auto-generated method stub
		return recordPlanMapperR.selectByExample(example);
	}

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public List<RecordPlan> selectAll() {
		// TODO Auto-generated method stub
		return recordPlanMapperR.selectAll();
	}

	@Override
	public RecordPlan selectByOther(RecordPlan entity) {
		// TODO Auto-generated method stub
		return recordPlanMapperR.selectOne(entity);
	}
	
	/**
	 * 根据条件分页获取列表
	 * @param baseQueryBean
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Override
	public PageInfo<RecordPlan> selectPageList(BaseQueryBean baseQueryBean,int pageIndex,int pageSize){
		PageHelper.startPage(pageIndex, pageSize, true);
		return new PageInfo<>(recordPlanMapperR.selectPageList(baseQueryBean));
	}
}