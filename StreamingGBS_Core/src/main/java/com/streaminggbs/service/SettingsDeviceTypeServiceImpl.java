package com.streaminggbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SettingsDeviceType;
import com.streaminggbs.interfaces.SettingsDeviceTypeService;
import com.streaminggbs.mapper.read.SettingsDeviceTypeMapperR;
import com.streaminggbs.mapper.write.SettingsDeviceTypeMapperW;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class SettingsDeviceTypeServiceImpl implements SettingsDeviceTypeService {

	@Autowired(required = false)
	private SettingsDeviceTypeMapperR settingsDeviceTypeMapperR;
	@Autowired(required = false)
	private SettingsDeviceTypeMapperW settingsDeviceTypeMapperW;

	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	@Override
	public int save(SettingsDeviceType entity) {
		// TODO Auto-generated method stub
		return settingsDeviceTypeMapperW.insertSelective(entity);
	}

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	@Override
	public int delete(Object key) {
		// TODO Auto-generated method stub
		return settingsDeviceTypeMapperW.deleteByPrimaryKey(key);
	}

	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	@Override
	public int updateAll(SettingsDeviceType entity) {
		// TODO Auto-generated method stub
		return settingsDeviceTypeMapperW.updateByPrimaryKey(entity);
	}

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	@Override
	public int updateNotNull(SettingsDeviceType entity) {
		// TODO Auto-generated method stub
		return settingsDeviceTypeMapperW.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	@Override
	public SettingsDeviceType selectByKey(Object key) {
		// TODO Auto-generated method stub
		return settingsDeviceTypeMapperR.selectByPrimaryKey(key);
	}

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	@Override
	public List<SettingsDeviceType> selectByExample(Object example) {
		// TODO Auto-generated method stub
		return settingsDeviceTypeMapperR.selectByExample(example);
	}

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public List<SettingsDeviceType> selectAll() {
		// TODO Auto-generated method stub
		return settingsDeviceTypeMapperR.selectAll();
	}

	@Override
	public SettingsDeviceType selectByOther(SettingsDeviceType entity) {
		// TODO Auto-generated method stub
		return settingsDeviceTypeMapperR.selectOne(entity);
	}
	
	/**
	 * 根据条件分页获取列表
	 * @param baseQueryBean
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Override
	public PageInfo<SettingsDeviceType> selectPageList(BaseQueryBean baseQueryBean,int pageIndex,int pageSize){
		PageHelper.startPage(pageIndex, pageSize, true);
		return new PageInfo<>(settingsDeviceTypeMapperR.selectPageList(baseQueryBean));
	}
}