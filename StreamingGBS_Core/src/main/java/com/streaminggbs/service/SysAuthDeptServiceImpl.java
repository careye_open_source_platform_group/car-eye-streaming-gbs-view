package com.streaminggbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SysAuthDept;
import com.streaminggbs.interfaces.GbInfoService;
import com.streaminggbs.interfaces.SysAuthDeptService;
import com.streaminggbs.mapper.read.SysAuthDeptMapperR;
import com.streaminggbs.mapper.write.DeviceChannelInfoMapperW;
import com.streaminggbs.mapper.write.DeviceInfoMapperW;
import com.streaminggbs.mapper.write.SysAuthDeptMapperW;
import com.streaminggbs.common.utils.IDManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class SysAuthDeptServiceImpl implements SysAuthDeptService {

	@Autowired(required = false)
	private SysAuthDeptMapperR sysAuthDeptMapperR;
	@Autowired(required = false)
	private SysAuthDeptMapperW sysAuthDeptMapperW;


	@Autowired
	private DeviceInfoMapperW deviceInfoMapperW;
	@Autowired
	private DeviceChannelInfoMapperW deviceChannelInfoMapperW;
	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	@Override
	public int save(SysAuthDept entity) {
		// TODO Auto-generated method stub
		return sysAuthDeptMapperW.insertSelective(entity);
	}

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	@Override
	public int delete(Object key) {
		// TODO Auto-generated method stub
		return sysAuthDeptMapperW.deleteByPrimaryKey(key);
	}

	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	@Override
	public int updateAll(SysAuthDept entity) {
		// TODO Auto-generated method stub
		return sysAuthDeptMapperW.updateByPrimaryKey(entity);
	}

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	@Override
	public int updateNotNull(SysAuthDept entity) {
		// TODO Auto-generated method stub
		return sysAuthDeptMapperW.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	@Override
	public SysAuthDept selectByKey(Object key) {
		// TODO Auto-generated method stub
		return sysAuthDeptMapperR.selectByPrimaryKey(key);
	}

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	@Override
	public List<SysAuthDept> selectByExample(Object example) {
		// TODO Auto-generated method stub
		return sysAuthDeptMapperR.selectByExample(example);
	}

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public List<SysAuthDept> selectAll() {
		// TODO Auto-generated method stub
		return sysAuthDeptMapperR.selectAll();
	}

	@Override
	public SysAuthDept selectByOther(SysAuthDept entity) {
		// TODO Auto-generated method stub
		return sysAuthDeptMapperR.selectOne(entity);
	}

	/**
	 * 根据条件分页获取列表
	 * @param baseQueryBean
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Override
	public PageInfo<SysAuthDept> selectPageList(BaseQueryBean baseQueryBean,int pageIndex,int pageSize){
		PageHelper.startPage(pageIndex, pageSize, true);
		return new PageInfo<>(sysAuthDeptMapperR.selectPageList(baseQueryBean));
	}

    @Override
    public List<SysAuthDept> getChildListByParentid(String parentid) {
        return sysAuthDeptMapperR.getChildListByParentid(parentid);
    }

    @Override
    public List<SysAuthDept> getSelectTreeDeptList(String deptid) {
        return sysAuthDeptMapperR.getSelectTreeDeptList(deptid);
    }
	@Override
	public boolean validGbId(String gbId, String id) {
		SysAuthDept paramGbInfo = new SysAuthDept();
		paramGbInfo.setGbId(gbId);
		SysAuthDept oldEntity = selectByOther(paramGbInfo);
		if(oldEntity != null && !oldEntity.getDeptid().equals(id)){
			return false;
		}
		return true;
	}
    @Override
    public int addDept(SysAuthDept entity) {
        //验证设备是否已存在
        boolean isValid = validGbId(entity.getGbId(), null);
        if(!isValid){
            log.info("机构编号已存在，parentid=" + entity.getParentid() + ", gbId=" + entity.getGbId());
            return -1;
        }
        entity.setDeptid(IDManager.nextId());
        //插入设备信息
        save(entity);

        return 1;
    }

    @Override
    public int editDept(SysAuthDept entity) {//验证设备是否已存在
        boolean isValid = validGbId(entity.getGbId(), entity.getDeptid());
        if(!isValid){
            log.info("机构编号已存在，parentid=" + entity.getParentid() + ", gbId=" + entity.getGbId());
            return -1;
        }
        //更新设备
        updateNotNull(entity);

        return 1;
    }

    @Override
    public int deleteDept(String userDeptId,String accessDeptId) {
        //删除国标总表、机构表、设备表、通道表信息
        //gbInfoService.deleteAllChildAndMOwn(deptid);
		SysAuthDept sysAuthDept =getRealUseDeptIdNew(userDeptId,accessDeptId);
		String deptIdNew =sysAuthDept.getDeptidnew();
		if(StringUtils.isNotEmpty(deptIdNew)) {
			deviceChannelInfoMapperW.deleteChannelChildAndOwnByDeptIdNew(deptIdNew);
			deviceInfoMapperW.deleteDeviceChildAndOwnByDeptIdNew(deptIdNew);
			sysAuthDeptMapperW.deleteDeptChildAndOwnByDeptIdNew(deptIdNew);
		}
		//sysAuthDeptMapperW.deleteByPrimaryKey(accessDeptId);

        return 1;
    }

	@Override
	public String getNextDeptIdNew(String parentid) {
		return sysAuthDeptMapperR.getNextDeptIdNew(parentid);
	}

	@Override
	public String getDeptIdNew(String parentid) {
		String deptIdNew = sysAuthDeptMapperR.getNextDeptIdNew(parentid);
		if (deptIdNew == null) {
			SysAuthDept sysAuthDept = selectByKey(parentid);
			deptIdNew = sysAuthDept.getDeptidnew() + "001";
		}
		return deptIdNew;
	}

	@Override
	public SysAuthDept getRealUseDeptIdNew(String userDeptId, String accessDeptId){
		//SysAuthUser user= LoginUserUtil.getUserEntity();
		SysAuthDept userSysAuthDeptEntity = new SysAuthDept();
		userSysAuthDeptEntity.setDeptid(userDeptId);
		userSysAuthDeptEntity = selectByOther(userSysAuthDeptEntity);
		if(StringUtils.isNotEmpty(accessDeptId)){
			SysAuthDept accessSysAuthDeptEntity = new SysAuthDept();
			accessSysAuthDeptEntity.setDeptid(accessDeptId);
			accessSysAuthDeptEntity = selectByOther(accessSysAuthDeptEntity);
			if(accessSysAuthDeptEntity!=null) {
				if (!userSysAuthDeptEntity.getDeptidnew().contains(accessSysAuthDeptEntity.getDeptidnew())){
					return accessSysAuthDeptEntity;
				}
			}
		}
		return userSysAuthDeptEntity;
	}
}
