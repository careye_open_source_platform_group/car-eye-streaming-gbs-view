package com.streaminggbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SysAuthLoginLog;
import com.streaminggbs.interfaces.SysAuthLoginLogService;
import com.streaminggbs.mapper.read.SysAuthLoginLogMapperR;
import com.streaminggbs.mapper.write.SysAuthLoginLogMapperW;
import com.streaminggbs.common.utils.DateUtil;
import com.streaminggbs.common.utils.IDManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class SysAuthLoginLogServiceImpl implements SysAuthLoginLogService {

	@Autowired(required = false)
	private SysAuthLoginLogMapperR sysAuthLoginLogMapperR;
	@Autowired(required = false)
	private SysAuthLoginLogMapperW sysAuthLoginLogMapperW;

	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	@Override
	public int save(SysAuthLoginLog entity) {
		// TODO Auto-generated method stub
		return sysAuthLoginLogMapperW.insertSelective(entity);
	}

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	@Override
	public int delete(Object key) {
		// TODO Auto-generated method stub
		return sysAuthLoginLogMapperW.deleteByPrimaryKey(key);
	}

	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	@Override
	public int updateAll(SysAuthLoginLog entity) {
		// TODO Auto-generated method stub
		return sysAuthLoginLogMapperW.updateByPrimaryKey(entity);
	}

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	@Override
	public int updateNotNull(SysAuthLoginLog entity) {
		// TODO Auto-generated method stub
		return sysAuthLoginLogMapperW.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	@Override
	public SysAuthLoginLog selectByKey(Object key) {
		// TODO Auto-generated method stub
		return sysAuthLoginLogMapperR.selectByPrimaryKey(key);
	}

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	@Override
	public List<SysAuthLoginLog> selectByExample(Object example) {
		// TODO Auto-generated method stub
		return sysAuthLoginLogMapperR.selectByExample(example);
	}

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public List<SysAuthLoginLog> selectAll() {
		// TODO Auto-generated method stub
		return sysAuthLoginLogMapperR.selectAll();
	}

	@Override
	public SysAuthLoginLog selectByOther(SysAuthLoginLog entity) {
		// TODO Auto-generated method stub
		return sysAuthLoginLogMapperR.selectOne(entity);
	}
	
	/**
	 * 根据条件分页获取列表
	 * @param baseQueryBean
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Override
	public PageInfo<SysAuthLoginLog> selectPageList(BaseQueryBean baseQueryBean,int pageIndex,int pageSize){
		PageHelper.startPage(pageIndex, pageSize, true);
		return new PageInfo<>(sysAuthLoginLogMapperR.selectPageList(baseQueryBean));
	}

    @Override
    public int addLoginLog(String deptid, String userid, String ip, int status) {
        SysAuthLoginLog loginLog = new SysAuthLoginLog();
        loginLog.setId(IDManager.nextId());
        loginLog.setDeptid(deptid);
        loginLog.setUserid(userid);
        loginLog.setLogindate(DateUtil.getDate());
        loginLog.setLoginip(ip);
        loginLog.setStatus(status);
        //登出时计算工作时长
        if (status == 2) {
            //根据userid查询最近一条登录信息
            SysAuthLoginLog lastLoginInfo = sysAuthLoginLogMapperR.getLastLoginInfo(userid);
            if(lastLoginInfo != null){
                loginLog.setWorkTime((int)(DateUtil.dateDiff(loginLog.getLogindate(),lastLoginInfo.getLogindate())/1000));
            }
        }
        save(loginLog);
        return 1;
    }
}