package com.streaminggbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.NodeMenu;
import com.streaminggbs.entity.SysAuthMenu;
import com.streaminggbs.interfaces.SysAuthMenuService;
import com.streaminggbs.mapper.read.SysAuthMenuMapperR;
import com.streaminggbs.mapper.write.SysAuthMenuMapperW;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SysAuthMenuServiceImpl implements SysAuthMenuService {

	@Autowired(required = false)
	private SysAuthMenuMapperR sysAuthMenuMapperR;
	@Autowired(required = false)
	private SysAuthMenuMapperW sysAuthMenuMapperW;

	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	@Override
	public int save(SysAuthMenu entity) {
		// TODO Auto-generated method stub
		return sysAuthMenuMapperW.insertSelective(entity);
	}

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	@Override
	public int delete(Object key) {
		// TODO Auto-generated method stub
		return sysAuthMenuMapperW.deleteByPrimaryKey(key);
	}

	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	@Override
	public int updateAll(SysAuthMenu entity) {
		// TODO Auto-generated method stub
		return sysAuthMenuMapperW.updateByPrimaryKey(entity);
	}

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	@Override
	public int updateNotNull(SysAuthMenu entity) {
		// TODO Auto-generated method stub
		return sysAuthMenuMapperW.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	@Override
	public SysAuthMenu selectByKey(Object key) {
		// TODO Auto-generated method stub
		return sysAuthMenuMapperR.selectByPrimaryKey(key);
	}

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	@Override
	public List<SysAuthMenu> selectByExample(Object example) {
		// TODO Auto-generated method stub
		return sysAuthMenuMapperR.selectByExample(example);
	}

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public List<SysAuthMenu> selectAll() {
		// TODO Auto-generated method stub
		return sysAuthMenuMapperR.selectAll();
	}

	@Override
	public SysAuthMenu selectByOther(SysAuthMenu entity) {
		// TODO Auto-generated method stub
		return sysAuthMenuMapperR.selectOne(entity);
	}
	
	/**
	 * 根据条件分页获取列表
	 * @param baseQueryBean
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Override
	public PageInfo<SysAuthMenu> selectPageList(BaseQueryBean baseQueryBean,int pageIndex,int pageSize){
		PageHelper.startPage(pageIndex, pageSize, true);
		return new PageInfo<>(sysAuthMenuMapperR.selectPageList(baseQueryBean));
	}

    @Override
    public List<NodeMenu> getAuthMenuList(String usergroupid) {
	    List<SysAuthMenu> authMenuList = sysAuthMenuMapperR.getAuthMenuList(usergroupid);
        List<NodeMenu> nodeMenuList = new ArrayList<>();
        if(authMenuList != null && !authMenuList.isEmpty()){
            toTree(nodeMenuList, authMenuList);
        }
        return nodeMenuList;
    }

    @Override
    public List<String> getAuthButtonList(String usergroupid) {
        return sysAuthMenuMapperR.getAuthButtonList(usergroupid);
    }

    /**
     * 菜单递归
     * @param nodeMenuList
     * @param menuList
     */
    private void toTree(List<NodeMenu> nodeMenuList, List<SysAuthMenu> menuList){
        List<SysAuthMenu> tempMenuList = new ArrayList<>();
        if(nodeMenuList.isEmpty()){
            for(SysAuthMenu menu : menuList){
                if(menu.getMenulevel() == 1){
                    NodeMenu node = new NodeMenu();
                    node.setId(menu.getMenuid());
                    node.setTitle(menu.getMenuname());
                    node.setPath(menu.getMenuaddr());
                    node.setIcon(menu.getImgurl());

                    nodeMenuList.add(node);
                }else{
                    tempMenuList.add(menu);
                }
            }
            toTree(nodeMenuList, menuList);
        }else{
            for(NodeMenu nodeMenu : nodeMenuList){
                for(SysAuthMenu menu : menuList){
                    if(nodeMenu.getId().equals(menu.getParentmenuid())){
                        NodeMenu node = new NodeMenu();
                        node.setId(menu.getMenuid());
                        node.setTitle(menu.getMenuname());
                        node.setPath(menu.getMenuaddr());
                        node.setIcon(menu.getImgurl());

                        nodeMenu.getChildren().add(node);
                    }else{
                        tempMenuList.add(menu);
                    }
                }

                if(!nodeMenu.getChildren().isEmpty() && !tempMenuList.isEmpty()){
                    toTree(nodeMenu.getChildren(), menuList);
                }
            }
        }
    }
}