package com.streaminggbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SysAuthUserGroupMenu;
import com.streaminggbs.interfaces.SysAuthUserGroupMenuService;
import com.streaminggbs.mapper.read.SysAuthUserGroupMenuMapperR;
import com.streaminggbs.mapper.write.SysAuthUserGroupMenuMapperW;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class SysAuthUserGroupMenuServiceImpl implements SysAuthUserGroupMenuService {

	@Autowired(required = false)
	private SysAuthUserGroupMenuMapperR sysAuthUserGroupMenuMapperR;
	@Autowired(required = false)
	private SysAuthUserGroupMenuMapperW sysAuthUserGroupMenuMapperW;

	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	@Override
	public int save(SysAuthUserGroupMenu entity) {
		// TODO Auto-generated method stub
		return sysAuthUserGroupMenuMapperW.insertSelective(entity);
	}

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	@Override
	public int delete(Object key) {
		// TODO Auto-generated method stub
		return sysAuthUserGroupMenuMapperW.deleteByPrimaryKey(key);
	}

	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	@Override
	public int updateAll(SysAuthUserGroupMenu entity) {
		// TODO Auto-generated method stub
		return sysAuthUserGroupMenuMapperW.updateByPrimaryKey(entity);
	}

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	@Override
	public int updateNotNull(SysAuthUserGroupMenu entity) {
		// TODO Auto-generated method stub
		return sysAuthUserGroupMenuMapperW.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	@Override
	public SysAuthUserGroupMenu selectByKey(Object key) {
		// TODO Auto-generated method stub
		return sysAuthUserGroupMenuMapperR.selectByPrimaryKey(key);
	}

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	@Override
	public List<SysAuthUserGroupMenu> selectByExample(Object example) {
		// TODO Auto-generated method stub
		return sysAuthUserGroupMenuMapperR.selectByExample(example);
	}

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public List<SysAuthUserGroupMenu> selectAll() {
		// TODO Auto-generated method stub
		return sysAuthUserGroupMenuMapperR.selectAll();
	}

	@Override
	public SysAuthUserGroupMenu selectByOther(SysAuthUserGroupMenu entity) {
		// TODO Auto-generated method stub
		return sysAuthUserGroupMenuMapperR.selectOne(entity);
	}
	
	/**
	 * 根据条件分页获取列表
	 * @param baseQueryBean
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Override
	public PageInfo<SysAuthUserGroupMenu> selectPageList(BaseQueryBean baseQueryBean,int pageIndex,int pageSize){
		PageHelper.startPage(pageIndex, pageSize, true);
		return new PageInfo<>(sysAuthUserGroupMenuMapperR.selectPageList(baseQueryBean));
	}
}