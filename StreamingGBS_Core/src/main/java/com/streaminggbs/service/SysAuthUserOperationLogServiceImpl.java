package com.streaminggbs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.entity.SysAuthUserOperationLogEntity;
import com.streaminggbs.interfaces.SysAuthUserOperationLogService;
import com.streaminggbs.mapper.write.SysAuthUserOperationLogW;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SysAuthUserOperationLogServiceImpl implements SysAuthUserOperationLogService {

	@Autowired(required = false)
	private SysAuthUserOperationLogW sysAuthUserOperationLogW;

	@Override
	public void save(SysAuthUserOperationLogEntity sysAuthUserOperationLogEntity) {
		sysAuthUserOperationLogW.insert(sysAuthUserOperationLogEntity);
	}

	@Override
	public PageInfo<SysAuthUserOperationLogEntity> findPage(int page, int limit,String startTime,String endTime) {
		PageHelper.startPage(page, limit, true);
		return new PageInfo<SysAuthUserOperationLogEntity>(sysAuthUserOperationLogW.findPage(startTime,endTime));
	}
}