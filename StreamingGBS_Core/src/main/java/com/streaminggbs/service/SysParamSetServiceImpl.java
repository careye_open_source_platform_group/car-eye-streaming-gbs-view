package com.streaminggbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.streaminggbs.common.base.BaseQueryBean;
import com.streaminggbs.entity.SysParamSet;
import com.streaminggbs.interfaces.SysParamSetService;
import com.streaminggbs.mapper.read.SysParamSetMapperR;
import com.streaminggbs.mapper.write.SysParamSetMapperW;

@Service
public class SysParamSetServiceImpl implements SysParamSetService {

	@Autowired(required = false)
	private SysParamSetMapperR sysParamSetMapperR;

	@Autowired(required = false)
	private SysParamSetMapperW sysParamSetMapperW;

	/**
	 * 保存单个对象
	 * 插入一条数据
	 * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
	 * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
	 * @param entity
	 * @return
	 */
	@Override
	public int save(SysParamSet entity) {
		return sysParamSetMapperW.insertSelective(entity);
	}

	/**
	 * 根据key删除记录，彻底删除
	 * @param key
	 * @return
	 */
	@Override
	public int delete(Object key) {
		return sysParamSetMapperW.deleteByPrimaryKey(key);
	}

	/**
	 * 更新所有字段
	 * @param entity
	 * @return
	 */
	@Override
	public int updateAll(SysParamSet entity) {
		return sysParamSetMapperW.updateByPrimaryKey(entity);
	}

	/**
	 * 根据主键进行更新
     * 只会更新不是null的数据
	 * @param entity
	 * @return
	 */
	@Override
	public int updateNotNull(SysParamSet entity) {
		return sysParamSetMapperW.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 根据主键进行查询,必须保证结果唯一
	 * 单个字段做主键时,可以直接写主键的值
	 * 联合主键时,key可以是实体类,也可以是Map
	 * @param key
	 * @return
	 */
	@Override
	public SysParamSet selectByKey(Object key) {
		return sysParamSetMapperR.selectByPrimaryKey(key);
	}

	/**
	 * 根据实体参数获取列表不分页
	 * @param example
	 * @return
	 */
	@Override
	public List<SysParamSet> selectByExample(Object example) {
		return sysParamSetMapperR.selectByExample(example);
	}

	/**
	 * 无参数获取列表不分页
	 * @return
	 */
	@Override
	public List<SysParamSet> selectAll() {
		return sysParamSetMapperR.selectAll();
	}

	@Override
	public SysParamSet selectByOther(SysParamSet entity) {
		return sysParamSetMapperR.selectOne(entity);
	}

	/**
	 * 根据条件分页获取列表
	 * @param baseQueryBean
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Override
	public PageInfo<SysParamSet> selectPageList(BaseQueryBean baseQueryBean,int pageIndex,int pageSize){
		PageHelper.startPage(pageIndex, pageSize, true);
		return new PageInfo<>(sysParamSetMapperR.selectPageList(baseQueryBean));
	}

    @Override
    public int deleteAll() {
        return sysParamSetMapperW.deleteAll();
    }

	@Override
	public SysParamSet pick() {
		return sysParamSetMapperR.selectByPrimaryKey("1");
	}
}