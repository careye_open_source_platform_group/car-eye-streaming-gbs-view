/**
 * 
 */
package com.streaminggbs.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.streaminggbs.entity.VideoServerInfo;
import com.streaminggbs.interfaces.VideoServerInfoService;
import com.streaminggbs.mapper.read.VideoServerInfoMapperR;
import com.streaminggbs.mapper.write.VideoServerInfoMapperW;

import tk.mybatis.mapper.entity.Example;

/**
 * @author Yisony
 *
 */
@Service
public class VideoServerInfoServiceImpl implements VideoServerInfoService {

	@Autowired
	private VideoServerInfoMapperR videoServerInfoMapperR;

	@Autowired
	private VideoServerInfoMapperW videoServerInfoMapperW;

	@Override
	public int save(VideoServerInfo entity) {
		entity.setCreateTime(new Date());
		return videoServerInfoMapperW.insertSelective(entity);
	}

	@Override
	public int delete(String id) {
		return videoServerInfoMapperW.deleteByPrimaryKey(id);
	}

	@Override
	public int updateAll(VideoServerInfo entity) {
		entity.setUpdateTime(new Date());
		return videoServerInfoMapperW.updateByPrimaryKey(entity);
	}

	@Override
	public int update(VideoServerInfo entity) {
		entity.setCreateTime(null);
		entity.setUpdateTime(new Date());
		return videoServerInfoMapperW.updateByPrimaryKeySelective(entity);
	}

	@Override
	public boolean enable(String id, boolean disable) {
		VideoServerInfo entity = new VideoServerInfo();
		entity.setId(id);
		entity.setEnabled(!disable);
		entity.setUpdateTime(new Date());
		return videoServerInfoMapperW.updateByPrimaryKeySelective(entity) > 0;
	}

	@Override
	public List<VideoServerInfo> selectAll() {
		return videoServerInfoMapperR.selectAll();
	}

	@Override
	public List<VideoServerInfo> selectByEnabled(boolean enabled) {
		Example example = new Example(VideoServerInfo.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("enabled", enabled);
		return videoServerInfoMapperR.selectByExample(example);
	}

	@Override
	public VideoServerInfo selectById(String id) {
		return videoServerInfoMapperR.selectByPrimaryKey(id);
	}

	@Override
	public VideoServerInfo pick() {
		List<VideoServerInfo> enabledServers = this.selectByEnabled(true);
		return enabledServers.isEmpty() ? null : enabledServers.get(0);
	}

}
